//
//  TableCell.swift
//  standings_sportsliga
//
//  Created by HoorPC on 17/07/2018.
//  Copyright © 2018 HoorPC. All rights reserved.
//

import UIKit
 class StandingTableCell: UITableViewCell {

    @IBOutlet var teamLabel: UILabel!
    
    @IBOutlet var GPLabel: UILabel!
    
    @IBOutlet var WLabel: UILabel!
    
    @IBOutlet var TLabel: UILabel!
    
    @IBOutlet var LLabel: UILabel!
    
    @IBOutlet var GFLabel: UILabel!
    
    @IBOutlet var GALabel: UILabel!
    
    @IBOutlet var GDLabel: UILabel!
    
    @IBOutlet var PTSLabel: UILabel!
    
    @IBOutlet var teamImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
