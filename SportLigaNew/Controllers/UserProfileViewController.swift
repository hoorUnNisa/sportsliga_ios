//
//  UserProfileViewController.swift
//  UserProfile
//
//  Created by Tahir on 6/11/18.
//  Copyright © 2018 Tahir. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

struct User:Codable{
    var id:Int
    var first_name:String
    var last_name:String
    var email: String
    var favorite: Int
    var image:String?
}

class UserProfileViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var imagePicker = UIImagePickerController()
    let user_id = UserDefaults.standard.getLogedInId()
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userGanderLabel: UILabel!
    @IBOutlet weak var userFavoriteLabel: UILabel!
    
    // button for profile image upload
    @IBOutlet weak var profilePicture: UIImageView!
    var user: [User] = []
    public func parseJSON(url : String) {
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
        guard let url = URL(string: url) else { return }
        let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            self.user = [try! JSONDecoder().decode(User.self, from: data)]
            //getting data from api
            self.showUser(first_name: self.user[0].first_name, last_name: self.user[0].last_name, email: self.user[0].email, id: self.user[0].id, favorite: self.user[0].favorite, image: self.user[0].image!)
            
        }
        session.resume()
    }
    // showing data on screen
    public func showUser(first_name: String, last_name: String , email: String, id: Int ,favorite: Int, image: String){
        DispatchQueue.main.async {
            self.userNameLabel.text = first_name + " " + last_name
            self.userEmailLabel.text = email
            self.userFavoriteLabel.text = "\(favorite)"
            do {
                let url = URL(string: "http://127.0.0.1:8000"+image)
                let data = try Data(contentsOf: url!)
                self.profilePicture.image = UIImage(data: data)
                UserDefaults.standard.setLogedInImage(image: image)
            }
            catch{
                print(error)
            }
            SVProgressHUD.dismiss()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        let url = "http://127.0.0.1:8000/api/user/show/\(user_id)"
        parseJSON(url: url)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width/2
        self.profilePicture.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // upload button
    @IBAction func uploadButton(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Open the camera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    func openGallary(){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            //self.profilePicture.image = editedImage
            var params = [String:AnyObject]()
            params["user_id"] = "\(self.user_id)" as  AnyObject
            
            // Grab your image from some variable or imageView. Here self.profileImage is a UIImage object
            if let imageData = UIImageJPEGRepresentation(editedImage, 1) {
                params["imageName"] = imageData as AnyObject
                APIManager.apiMultipart(serviceName: "http://127.0.0.1:8000/api/user/upload", parameters: params, completionHandler: { (response:JSON?, error:NSError?) in
                    //response handle
                    var link = response!["result"]
                    UserDefaults.standard.setLogedInImage(image: "\(link)")
                    SVProgressHUD.show(withStatus: "Please Wait")
                    self.profilePicture.image = editedImage
                    SVProgressHUD.dismiss()
                })
            } else {
                print("Image problem")
            }
            
            
            //Dismiss the UIImagePicker after selection
            picker.dismiss(animated: true, completion: nil)
        }
        
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            print("Cancelled")
        }
        
    }
}
