//
//  TopPlayerTableViewController.swift
//  SportLigaNew
//
//  Created by Hoor on 02/10/2018.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
class PollTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var pollTableView: UITableView!
    var pollArray : [Poll] = []
    
    @IBOutlet weak var noDataImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pollTableView.delegate = self
        pollTableView.dataSource = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Polls"
        print("poll controller")
    
        fetchPoll()
    }
    
    /* Fetch all poll and parse it */
    
    @objc func loadList(){
        fetchPoll()
    }
    func fetchPoll(){
        pollArray = []
        SVProgressHUD.show(withStatus: "Please Wait")
        let urlString = "http://127.0.0.1:8000/api/searchPoll"
        var userArray : Dictionary = [String: Any]()
        print(UserDefaults.standard.getSeasonId())
        print(UserDefaults.standard.getPollTitle())
        if(UserDefaults.standard.getSeasonId().isEmpty){
            userArray["season_id"] = ""
        }else{
            userArray["season_id"] = UserDefaults.standard.getSeasonId()
        }
        if(UserDefaults.standard.getPollTitle().isEmpty){
            userArray["poll_title"] = ""
        }else{
            userArray["poll_title"] = UserDefaults.standard.getPollTitle()
        }
        
        Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseArray { (response : DataResponse<[Poll]>) in
            if(response.result.value?.isEmpty)!{
                self.noDataImage.isHidden = false
            }
            if(response.response?.statusCode == 200){
                self.pollArray.append(contentsOf: response.result.value!)
                DispatchQueue.main.async {
                    self.pollTableView.reloadData()
                }
                SVProgressHUD.dismiss()
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pollArray.count
    }
    
    /* Show poll in table cell */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PollCell", for: indexPath) as! PollTableViewCell
        cell.PlayerOfPoll.text = pollArray[indexPath.row].poll_title
        cell.pollLastDate.text = pollArray[indexPath.row].poll_end_date
        print("poll_id")
        print(pollArray[indexPath.row].poll_id!)
        return cell
    }
    
    /* click on poll to show single poll screen */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        print("poll id at click")
        print(pollArray[indexPath.row].poll_id)
        let voteController = storyboard.instantiateViewController(withIdentifier: "VoteViewController") as! VoteViewController
        voteController.poll_id = pollArray[indexPath.row].poll_id!
        self.navigationController?.pushViewController(voteController, animated: true)
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

