//
//  PlayerTableViewController.swift
//  PlayerScreen
//
//  Created by hidayatullah on 18/07/2018.
//  Copyright © 2018 hidayatullah. All rights reserved.
//

import UIKit
import SVProgressHUD

struct Players: Codable{
    var id: Int
    var team_id: Int
    var name: String
    var Image:String?
    var team_name: String
    var favorite: Int
    var position: String
    var date_of_signing: String?
    var date_of_birth: String?
    var height : String?
    var weight: String?
    var income : String?
    var nationality : String?
}

class PlayerTableViewController: UITableViewController {
    var players: [Players] = []
    var teamId = Int()
    @IBOutlet weak var nodataView: UIView!
    @IBOutlet weak var noDataImg: UIImageView!
    @IBOutlet weak var noDatalabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nodataView.isHidden = true
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Players"
        //calling check function to check if user is login or not
//        ProgressHUD.show("Please Wait", interaction: false)

        SVProgressHUD.show(withStatus: "Please Wait")
        DispatchQueue.main.async {
            self.check()
        }
    }
    // function to check user is login or not
    @objc func check(){
        let getData =  tabBarController as! TeamTabBarViewController
        teamId = getData.getTeamId
        let user_id = UserDefaults.standard.getLogedInId()
        if(user_id > 0){
            let Url = "http://127.0.0.1:8000/api/playersbyteam/\(teamId)/\(user_id)"
            print(Url)
            self.parseApi(Url : Url)
        }
        else {
            let secUrl = "http://127.0.0.1:8000/api/playersbyteam/\(teamId)"
            print(secUrl)
            self.parseApi(Url : secUrl)
        }
    }
    // function to parse data from api
    @objc func parseApi(Url : String){
        guard let url = URL(string: Url) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                let model = try decoder.decode([Players].self, from:
                    dataResponse) //Decode JSON Response Data
//                print(model)
                self.players = model
                DispatchQueue.main.async {

//                    ProgressHUD.show("Please Wait", interaction: false)
                    SVProgressHUD.show(withStatus: "Please Wait")
                    self.tableView.reloadData()
                    SVProgressHUD.dismiss()
                }
                //                SVProgressHUD.dismiss()
                
            } catch let parsingError {
                print("Error", parsingError)
            }
            SVProgressHUD.dismiss()
        }
        task.resume()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //return total count of players
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if players.count == 0 {
            return 1
        }
        else{
        return players.count
        }
    }
    //cell height for each row
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    //function to set values to each cell elements
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerData", for: indexPath) as! PlayerTableViewCell
        if players.count == 0{
            nodataView.isHidden = false
            cell.isHidden       = true
            
        }
        else{
            nodataView.isHidden  = true
            cell.isHidden        = false
        if(players[indexPath.row].Image != nil){
        let imageUrlString = players[indexPath.row].Image
            if let url = URL(string : imageUrlString!){
            do {
                let data = try Data(contentsOf: url)
                cell.playerImage.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
                }
            }
        }
        if(players[indexPath.row].favorite == 1){
            cell.playerButton.setImage(UIImage(named: "filled red heart"), for: .normal)
        }
        else{
            cell.playerButton.setImage(UIImage(named: "red heart"), for: .normal)
        }
        cell.playerName.text   = "\(players[indexPath.row].name)"
        cell.teamName.text   = "\(players[indexPath.row].team_name)"
        cell.playerButton.tag =  players[indexPath.row].id
        cell.playerButton.addTarget(self, action: #selector(Button_favorite(sender:)), for: .touchUpInside)
        }
            return cell
    }

    // button functionality to check user login or not to add/remove favorite
    @objc func Button_favorite(sender: UIButton){
        print(sender.tag)
        SVProgressHUD.show(withStatus: "Please Wait")
        sender.setImage(UIImage(named: "filled red heart.png"), for: .normal)
        sender.isUserInteractionEnabled = true
        let player_id = sender.tag
        let user_id = UserDefaults.standard.getLogedInId()
        let type = "player"
        if(user_id > 0){
            SVProgressHUD.show(withStatus: "Please Wait")
            favorite(player_id: player_id , user_id: user_id, type: type)
        }
        else{
            self.alert(title: "Alert", message: "Please Login or register")
        }
    }
    // alert function to show message
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }

    //function to send request to add/ remove favorite
    public func favorite(player_id: Int , user_id : Int , type: String){
        
        var favArray : Dictionary = [String:String]()
        favArray["id"] = "\(player_id)"
        favArray["user_id"] = "\(user_id)"
        favArray["type"] = type
        
        guard let url = URL(string: "http://127.0.0.1:8000/api/favorite") else { return }
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: favArray, options: []) else { return }
        request.httpBody = httpbody;
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response{
                print(response)
            }
            if let data = data{
                do{
                    // updating tableview if user add/remove favorite
                    let parsedData = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    print(parsedData)
                    let key = "result"
                    let stateCode = parsedData[key] as? Int ?? Int(parsedData[key] as? String ?? "")
                    print(stateCode!)
                    let result = stateCode!
                    if(result == 0){
                        DispatchQueue.main.async {
                            self.check()
                        }
                    }
                    if(result == 1){
                        DispatchQueue.main.async {
                            self.check()
                        }
                    }
                }catch{
                    print(error)
                }
            }
            }.resume()
    }
    @objc func loadMoreTeams(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let playerController = storyboard.instantiateViewController(withIdentifier: "PlayerProfileViewController") as! PlayerProfileViewController
        playerController.getPlayerId = players[indexPath.row].id
        self.navigationController?.pushViewController(playerController, animated: false)
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

