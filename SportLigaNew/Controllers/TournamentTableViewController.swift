//
//  TournamentTableViewController.swift
//  SportsLiga
//
//  Created by Ali Iqbal on 6/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SVProgressHUD
//import Alamofire
struct Tournament:Codable
{
    var id: Int
    var name: String
    var country: String
    var badge: String
    var favorite: Int
}

class TournamentTableViewController: UITableViewController {
    @IBOutlet var tbl_tournaments: UITableView!
    var tournament: [Tournament] = []
    var timer = Timer()
    let user_id = UserDefaults.standard.getLogedInId()
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataImg: UIImageView!
    @IBOutlet weak var noDataLabel: UILabel!
    override func awakeFromNib() {
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedStringKey.font:UIFont(name: "Arial", size: 15)]
        appearance.setTitleTextAttributes(attributes, for: .normal)
    }
    
    @objc func check(){
        if(user_id > 0){
            let url = "http://127.0.0.1:8000/api/tournaments/"+"\(user_id)"
            self.parseApi(url: url)
        }
        else {
            let url = "http://127.0.0.1:8000/api/tournaments"
            self.parseApi(url: url)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Alamofire.request("http://127.0.0.1:8000/api/tournaments").responseJSON { response in
//            let result = response.result
//            print(result.value)
//
//        }
        noDataView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.topItem?.title = "Tournaments"
        SVProgressHUD.show(withStatus: "Please Wait")
        self.timer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(TournamentTableViewController.check), userInfo: nil, repeats: true)
        DispatchQueue.main.async {
            self.check()
        }
        
        
    }
    
    /* fetch tournaments and parse the tournament data */
    
    @objc func parseApi(url : String){
        
        guard let url = URL(string: url) else { return 	}
        let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            //            print(data)
            self.tournament = try! JSONDecoder().decode([Tournament].self, from: data)
            //print(self.tournament)
            DispatchQueue.main.async {
                self.tbl_tournaments.reloadData()
                SVProgressHUD.dismiss()
            }
            if self.tournament.count == 0{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = false
                }
                
            }else{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = true
                }
                
            }
            //            SVProgressHUD.dismiss()
        }
        session.resume()
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return tournament.count
        
        }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TournamentCell", for: indexPath) as! TournamentTableViewCell
      
        
        
        let imageUrlString = "http://127.0.0.1:8000"+tournament[indexPath.row].badge
        print(imageUrlString)
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                cell.TournamentLogo.image = UIImage(data: data)
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        if(tournament[indexPath.row].favorite == 1){
            cell.tournamentButton.setImage(UIImage(named: "filled red heart"), for: .normal)
        }
        else{
            cell.tournamentButton.setImage(UIImage(named: "red heart"), for: .normal)
        }
        print(tournament[indexPath.row].id)
        cell.TournamentName.text = "\(tournament[indexPath.row].name)"
        cell.TournamentLocation.text = "\(tournament[indexPath.row].country)"
        cell.tournamentButton.tag =  tournament[indexPath.row].id
        cell.tournamentButton.addTarget(self, action: #selector(Button_favorite(sender:)), for: .touchUpInside)
        
            return cell
    }
    
    
    /* present next view contoller to tab on tournament s*/
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TournamentTabBarViewController") as! TournamentTabBarViewController
        tabBarController.getTournamentId = tournament[indexPath.row].id
        if let viewControllers = tabBarController.viewControllers,
            let TeamDC = viewControllers.first as? TeamTableViewController {
            TeamDC.getTournamentId = tournament[indexPath.row].id
            
        }
        navigationController?.pushViewController(tabBarController, animated: false)
    }
    
    @objc func Button_favorite(sender: UIButton){
        //        print(sender.tag)
        
        print("button clicked")
        let tournament_id:Int = sender.tag
        let user_id = UserDefaults.standard.getLogedInId()
        let type = "tournament"
        if(user_id > 0){
            SVProgressHUD.show(withStatus: "Please Wait")
            favorite(tournament_id: tournament_id, user_id: user_id, type: type)
        }
        else{
            self.alert(title: "Alert", message: "Please Login or register")
            
        }
    }
    
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    public func favorite(tournament_id: Int , user_id : Int , type: String){
        var favArray : Dictionary = [String:String]()
        favArray["id"] = "\(tournament_id)"
        favArray["user_id"] = "\(user_id)"
        favArray["type"] = type
        
        guard let url = URL(string: "http://127.0.0.1:8000/api/favorite") else { return }
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: favArray, options: []) else { return }
        request.httpBody = httpbody;
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response{
                print(response)
            }
            if let data = data{
                do{
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    print(parsedData)
                    let key = "result"
                    let stateCode = parsedData[key] as? Int ?? Int(parsedData[key] as? String ?? "")
                    print(stateCode!)
                    let result = stateCode!
                    
                    if(result == 0){
                        DispatchQueue.main.async {
                            self.check()
                        }
                    }
                    if(result == 1){
                        DispatchQueue.main.async {
                            self.check()
                        }
                    }
                }catch{
                    print(error)
                }
            }
            }.resume()
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


