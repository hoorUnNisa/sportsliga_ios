//
//  PlayerProfileViewController.swift
//  PlayerProfileScreen
//
//  Created by Ali Iqbal on 7/20/18.
//  Copyright © 2018 Hoor. All rights reserved.
//

import UIKit
import SVProgressHUD

struct PlayerDetails: Codable{
    var name: String
    var team_name: String
    var position: String?
    var date_of_signing: String?
    var date_of_birth: String?
    var height : String?
    var weight: String?
    var income : String?
    var nationality: String?
    var thumb_pic: String?
    var description: String?
}


class PlayerProfileViewController: UIViewController {
    var getPlayerId = Int()
    var playerDetail: [PlayerDetails] = []
    //set label outlets of player profile screen
    @IBOutlet weak var playerTeamLabel: UILabel!
    @IBOutlet weak var playerHeightLabel: UILabel!
    @IBOutlet weak var playerJoiningDateLabel: UILabel!
    @IBOutlet weak var playerWeightLabel: UILabel!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerIncome: UILabel!
    @IBOutlet weak var playerNationalityLabel: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    @IBOutlet var playerImg: UIImageView!
    @IBOutlet weak var playerDiscriptionLabel: UITextView!
    
    
//    @IBOutlet weak var playerViewContainer: UIView!
//    var views: [UIView]!
    //
    //    var simpleView1: UIView!
    //    var simpleView2: UIView!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
//        views = [UIView]()
//        views.append(PlayerVC1().view)
//        views.append(PlayerVC2().view)
        
//        for v in views {
//            playerViewContainer.addSubview(v)
//        }
//        playerViewContainer.bringSubview(toFront: views[0])
        self.navigationController?.navigationBar.topItem?.title = "Profile"
        playerImg.layer.cornerRadius = playerImg.frame.size.width/2
        playerImg.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {

        // calling api for fetching Player Data
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
        let Url = "http://127.0.0.1:8000/api/player/show/\(self.getPlayerId)"
        if(getPlayerId > 0){
        self.parseJSON(url: Url)
        }
    }
    //Api parsing method
    public func parseJSON(url: String) {
        guard let url = URL(string: url) else { return }
        let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            self.playerDetail = try! JSONDecoder().decode([PlayerDetails].self, from: data)
            print(self.playerDetail)
            DispatchQueue.main.async {
                var pPosition, pDateOfJoining, pNationality, pHeight, pWeight,pDOB,pDescription : String
                if(self.playerDetail[0].position != nil){
                    pPosition = self.playerDetail[0].position!
                }
                else{
                    pPosition = "N/A"
                }
                if(self.playerDetail[0].date_of_signing != nil){
                    pDateOfJoining = self.playerDetail[0].date_of_signing!
                }
                else{
                    pDateOfJoining = "N/A"
                }
                if(self.playerDetail[0].nationality != nil){
                    pNationality = self.playerDetail[0].nationality!
                }
                else{
                    pNationality = "N/A"
                }
                if(self.playerDetail[0].height != nil){
                    pHeight = self.playerDetail[0].height!
                }
                else{
                    pHeight = "N/A"
                }
                if(self.playerDetail[0].weight != nil){
                    pWeight = self.playerDetail[0].weight!
                }
                else{
                    pWeight = "N/A"
                }
                if(self.playerDetail[0].date_of_birth != nil){
                    pDOB = self.playerDetail[0].date_of_birth!
                }
                else{
                    pDOB = "N/A"
                }
                if(self.playerDetail[0].description != nil){
                    pDescription = self.playerDetail[0].description!
                }
                else{
                    pDescription = "N/A"
                }
                self.showPlayer(name: self.playerDetail[0].name, position: pPosition, teamName: self.playerDetail[0].team_name, dateOfJoining: pDateOfJoining, nationality: pNationality, height: pHeight, weight: pWeight, income: pDOB, description: pDescription, profilePic: self.playerDetail[0].thumb_pic!)

                SVProgressHUD.dismiss()
            }
        }
        session.resume()
        
    }
    //calling function to show player data
    func showPlayer(name:String, position:String, teamName:String, dateOfJoining:String, nationality:String, height:String, weight:String, income: String, description:String , profilePic : String){
        self.playerName.text = name
        self.playerPosition.text = position
        self.playerTeamLabel.text = teamName
        self.playerJoiningDateLabel.text = dateOfJoining
        self.playerNationalityLabel.text = nationality
        self.playerHeightLabel.text = height
        self.playerWeightLabel.text = weight
        self.playerIncome.text = income
        self.playerDiscriptionLabel.text = description
        let imageUrlString = profilePic
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                playerImg.image = UIImage(data: data)

            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func switchViewAction(_ sender: UISegmentedControl) {
//
////        self.playerViewContainer.bringSubview(toFront: views[sender.selectedSegmentIndex])
//        //        switch sender.selectedSegmentIndex {
//        //        case 0:
//        //            playerViewContainer.bringSubview(toFront: simpleView1)
//        //            break
//        //        case 1:
//        //            playerViewContainer.bringSubview(toFront: simpleView2)
//        //            break
//        //        default:
//        //            break
//        //        }
//
//    }
    
}
