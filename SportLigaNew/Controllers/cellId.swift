//
//  cellId.swift
//  SportLigaNew
//
//  Created by Tahir on 8/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
protocol cellId {
    func idFromCell(teamName: String)
}
