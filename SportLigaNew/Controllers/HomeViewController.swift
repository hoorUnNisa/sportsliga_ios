//
//  HomeViewController.swift
//  SportsLiga
//
//  Created by Ali Iqbal on 7/22/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController {

    @IBOutlet weak var homeLoginBtn: UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showProfile),
                                               name: NSNotification.Name("ShowProfile"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showNotification),
                                               name: NSNotification.Name("ShowNotification"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showLogin),
                                               name: NSNotification.Name("HomeToLogin"),
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userLogedIn = UserDefaults.standard.isLogedIn()
        print(userLogedIn)
        if userLogedIn == true{
            
            self.homeLoginBtn.tintColor = UIColor.clear
            self.homeLoginBtn.isEnabled = false
            
        }else{
            self.menuButton.tintColor = UIColor.clear
            self.menuButton.isEnabled = false
        }
    }
    
    /* Home to Login page notification method */
    
    @IBAction func homeToLogin(_ sender: Any) {
        
        self.performSegue(withIdentifier: "HomeToLogin", sender: nil)
    }
    
    /* Show user profile page notification method */
    @objc func showProfile() {
        performSegue(withIdentifier: "ShowProfile", sender: nil)
    }
    
    /* Show Notification page notification method */
    
    @objc func showNotification() {
        performSegue(withIdentifier: "ShowNotification", sender: nil)
    }
    
    /* Home to Login page notification method */

    @objc func showLogin() {
        performSegue(withIdentifier: "HomeToLogin", sender: nil)
    }
    
    /* Show or hide side menu */
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name("ReloadSlideMenu"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
