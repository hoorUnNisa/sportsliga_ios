//
//  UpComingTableViewCell.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class UpComingTableViewCell: UITableViewCell {
    @IBOutlet weak var homeTeamImage: UIImageView!
    
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var matchTime: UILabel!
    @IBOutlet weak var matchDate: UILabel!
    @IBOutlet weak var homeTeamName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
