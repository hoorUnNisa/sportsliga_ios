//
//  ViewController.swift
//  practicfe
//
//  Created by HoorPC on 10/07/2018.
//  Copyright © 2018 HoorPC. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import ObjectMapper
import GoogleSignIn
import SVProgressHUD
extension UITextField {
    //paddding of textFields in registration form
    func setPadding(){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    //bottom split line of textFields in registration form
    func setBottomBorder(){
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

class RegisterationViewController: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
//Input Text Field
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    //Labels
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var cofirmPassword: UITextField!
    @IBOutlet var email: UITextField!
    //Buttons
    @IBOutlet weak var FacebookBtn: UIButton!
    
    /*  click login with google button */
    
    @IBOutlet weak var GoogleBtn: UIButton!
    @IBAction func loginWithGoogle(_ sender: Any) {
         GIDSignIn.sharedInstance().signIn()
    }
    
    /* After click login with google button parse data of user */
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
//            ProgressHUD.show()
            SVProgressHUD.show()
            var userArray : Dictionary = [String: String]()
            userArray["provider_user_id"] = user.userID
            userArray["first_name"] = user.profile.givenName
            userArray["last_name"] = user.profile.familyName
            userArray["email"] = user.profile.email
            userArray["provider"] = "google"
            print(userArray)
            GIDSignIn.sharedInstance().signOut()
            let urlString = "http://127.0.0.1:8000/api/socialLogin"
            Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                (response : DataResponse<SocialUser>) in
                let socialUser = response.result.value
                UserDefaults.standard.setIsLogedIn(value: true)
                UserDefaults.standard.setLogedInId(id: socialUser!.user_id!)
                UserDefaults.standard.setLogedInFirstName(first_name: socialUser!.firstName!)
                UserDefaults.standard.setLogedInLastName(last_name: socialUser!.lastName! )
                UserDefaults.standard.setLogedInEmail(email: socialUser!.email! )
                UserDefaults.standard.synchronize()
//                ProgressHUD.dismiss()
                SVProgressHUD.dismiss()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let containerVC = storyboard.instantiateViewController(withIdentifier: "ContainerVC") as! ContainerVC
                self.present(containerVC, animated: true, completion: nil)
            }
        }
        
    }
    /* After click login with facebook button parse data of user */
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
//                    ProgressHUD.show()
                    SVProgressHUD.show()
                    let res = result as! Dictionary<String, String>
                    var userArray : Dictionary = [String: String]()
                    userArray["last_name"] = res["last_name"]
                    userArray["first_name"] = res["first_name"]
                    userArray["email"] = res["email"]
                    userArray["provider_id"] = res["id"]
                    userArray["provider"] = "facebook"
                    //print(res["email"]!)
                    let urlString = "http://127.0.0.1:8000/api/socialLogin"
                    Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                        (response : DataResponse<SocialUser>) in
                        let socialUser = response.result.value
                        UserDefaults.standard.setIsLogedIn(value: true)
                        UserDefaults.standard.setLogedInId(id: socialUser!.user_id!)
                        UserDefaults.standard.setLogedInFirstName(first_name: socialUser!.firstName!)
                        UserDefaults.standard.setLogedInLastName(last_name: socialUser!.lastName! )
                        UserDefaults.standard.setLogedInEmail(email: socialUser!.email! )
                        UserDefaults.standard.synchronize()
//                        ProgressHUD.dismiss()
                        SVProgressHUD.dismiss()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let containerVC = storyboard.instantiateViewController(withIdentifier: "ContainerVC") as! ContainerVC
                        self.present(containerVC, animated: true, completion: nil)
                    }
                }
            })
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.FacebookBtn?.layer.borderColor = UIColor.blue.cgColor
        self.FacebookBtn?.layer.borderColor = UIColor(red: 50/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        self.FacebookBtn?.layer.borderWidth = 2
        self.FacebookBtn?.layer.cornerRadius = 25;
        
//        self.GoogleBtn?.layer.borderColor = UIColor.blue.cgColor
//        self.GoogleBtn?.layer.borderColor = UIColor(red: 50/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
//        self.GoogleBtn?.layer.borderWidth = 2
//        self.GoogleBtn?.layer.cornerRadius = 25;

        
        self.GoogleBtn?.layer.cornerRadius = 25;
        self.GoogleBtn?.layer.borderColor = UIColor.orange.cgColor
        self.GoogleBtn?.layer.borderColor = UIColor(red: 255/255.0, green: 110/255.0, blue: 126/255.0, alpha: 1.0).cgColor
        self.GoogleBtn?.layer.borderWidth = 2
        
        
        GIDSignIn.sharedInstance().delegate = self
        self.firstNameTextField.delegate = self;
        self.lastNameTextField.delegate = self;
        self.emailTextField.delegate = self;
        self.passwordTextField.delegate = self;
        self.confirmPasswordTextField.delegate = self;
        
        // Do any additional setup after loading the view, typically from a nib.
        //Calling Function to set padding and bottomborder of textFields
        firstName.setPadding()
        firstName.setBottomBorder()
        lastName.setPadding()
        lastName.setBottomBorder()
        password.setPadding()
        password.setBottomBorder()
        cofirmPassword.setPadding()
        cofirmPassword.setBottomBorder()
        email.setPadding()
        email.setBottomBorder()
    }
    
  
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    // for Done Button in virtual keyboard to finish typing
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* back to login screen */
    
    @IBAction func backToLogin(_ sender: Any) {
        self.performSegue(withIdentifier: "RegisterShowLogin", sender: nil)
    }
    
    
    //for restriction on the input types
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String)->Bool {
//
//        let allowedCharacters = CharacterSet.letters
//        let characterSet = CharacterSet(charactersIn: string)
//        return allowedCharacters.isSuperset(of: characterSet)
//    }

    
    //Validation Of Registraion form
    @IBAction func registerButton(_ sender: AnyObject) {
        
        let userFirstName = firstNameTextField.text
        let userLastName = lastNameTextField.text
        let userEmail = emailTextField.text
        let userPassword = passwordTextField.text
        let userConfirmPassword = confirmPasswordTextField.text
        
        let isFirstNameValid = isValidName(NameString: userFirstName!)
        let isLastNameValid = isValidName(NameString: userLastName!)
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: userEmail!)
        let isPasswordValid = isValidPassword(passwordString: userPassword!)
        
        //Set Up Registration if data in Registration form fields is valid
        if isFirstNameValid && isLastNameValid && isEmailAddressValid && isPasswordValid{
            //print("every thing is valid");
            var userArray : Dictionary = [String:String]()
            userArray["first_name"] = userFirstName
            userArray["last_name"] = userLastName
            userArray["email"] = userEmail
            userArray["password"] = userPassword
//            ProgressHUD.show("Please Wait", interaction: false)
            SVProgressHUD.show(withStatus: "Please Wait")
            guard let url = URL(string: "http://127.0.0.1:8000/api/register") else { return }
            var request = URLRequest(url:url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            guard let httpbody = try? JSONSerialization.data(withJSONObject: userArray, options: []) else { return }
            request.httpBody = httpbody;
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let response = response{
                    print(response)
                }
                if let data = data{
                    do{
                        let user : NSDictionary = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if let emailExits = user["emailExits"]{
                            print(emailExits)
//                            ProgressHUD.dismiss()
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Email", message: (emailExits as! String), preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                
                                self.present(alert, animated: true)
                                
                            }
                        }
                        if let emailSend = user["emailSend"]{
                            
//                            ProgressHUD.dismiss()
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Email", message: (emailSend as! String), preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){
                                    UIAlertAction in
                                    self.performSegue(withIdentifier: "RegisterShowLogin", sender: nil)
                                })
                                
                                self.present(alert, animated: true)
                                
                            }
                        }
                    }catch{
                        print(error)
//                        ProgressHUD.dismiss()
                        SVProgressHUD.dismiss()
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Email", message: "Connection Error", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            
                            self.present(alert, animated: true)
                            
                        }
                    }
                }
            }.resume()
            
            
            
            
            
        }
        //Alert on Validation of Empty Fields of Registration Form
        if((userFirstName?.isEmpty)! || (userLastName?.isEmpty)!||(userEmail?.isEmpty)!||(userPassword?.isEmpty)!||(userConfirmPassword?.isEmpty)!){
            let myAlert = UIAlertController(title:"Alert",message:"All fields are required to fill in",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        //Alert on Validation of First Name
        if isFirstNameValid
        {
            print("First Name is valid")
        }else {
            let myAlert = UIAlertController(title:"Alert",message:"Firstname should have atleast 3 alphabets.",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        //Alert on Validation of Last Name
        if isLastNameValid
        {
            print("Last Name is valid")
        }else {
            let myAlert = UIAlertController(title:"Alert",message:"Lastname should have atleast 3 alphabets.",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        //Alert on Validation of EmailAddress
        if isEmailAddressValid
        {
            print("Email address is valid")
        }else {
            let myAlert = UIAlertController(title:"Alert",message:"Email is not valid. PLease try again",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        //Alert on VAlidation of Password
        if isPasswordValid
        {
            print("Password is valid")
        }else {
            let myAlert = UIAlertController(title:"Alert",message:"Password should be in 6-20 range.",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        
      
//     Alert on  Validation of Confirm Password
        
        if(userPassword != userConfirmPassword){
            
            let myAlert = UIAlertController(title:"Alert",message:"Password do not match. PLease try again",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
            return
        }
        
    }
    // Spicify Data for Email validation
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Za-z0-9.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do{
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))

            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invald regex: \(error.localizedDescription)")
            returnValue = false
        }
        return returnValue
    }
    // Spicify Data for name validation
    func isValidName(NameString: String) -> Bool {
        var returnValue = true
        let nameRegEx = "[A-Za-z]{3,4}"
        
        do{
            let regex = try NSRegularExpression(pattern: nameRegEx)
            let nsString = NameString as NSString
            let results = regex.matches(in: NameString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invald regex: \(error.localizedDescription)")
            returnValue = false
        }
        return returnValue
    }
    // Spicify Data for password validation
    func isValidPassword(passwordString: String) -> Bool {
        var returnValue = true
        let passwordRegEx = "[A-Za-z0-9]{6,20}"

        do{
            let regex = try NSRegularExpression(pattern: passwordRegEx)
            let nsString = passwordString as NSString
            let results = regex.matches(in: passwordString, range: NSRange(location: 0, length: nsString.length))

            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invald regex: \(error.localizedDescription)")
            returnValue = false
        }
        return returnValue
    }
}

