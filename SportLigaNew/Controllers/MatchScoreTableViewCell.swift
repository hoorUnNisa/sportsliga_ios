//
//  MatchScoreTableViewCell.swift
//  SportsLiga
//
//  Created by Ali Iqbal on 7/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class MatchScoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var Replybtn: UIButton!
    //    @IBOutlet weak var replyText: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var replyOf: UIButton!
    //    @IBOutlet weak var Replybtn: UIButton!
   
    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var userName: UILabel!
    var commentId:Int?
    var userId:Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

