//
//  TeamTabBarViewController.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 8/7/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class TeamTabBarViewController: UITabBarController, UITabBarControllerDelegate{
    var getTeamId = Int()
    var getImgUrl = String()
    var getTeamName   =  String()
    var getTeamCountry = String()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tabBarController?.delegate = self
        self.delegate = self

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //to swith between tabs of player screen
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 1 {
        
            let firstVC = UITableViewController() as? PlayerTableViewController
            firstVC?.teamId = getTeamId
        
    }
    }
//
    // UITabBarControllerDelegate
  
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //
        
        
      
    
}
}
