//
//  SideMenuVC.swift
//  Side Menu
//
//  Created by Kyle Lee on 8/6/17.
//  Copyright © 2017 Kyle Lee. All rights reserved.
//

import UIKit

class SideMenuVC: UITableViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadSlideMenu(notification:)),
                                               name: NSNotification.Name("ReloadSlideMenu"),
                                               object: nil)
       
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width/2
        self.profileImage.clipsToBounds = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.isLogedIn() == true{
           tableView.reloadData()
            do {
                if(UserDefaults.standard.getLogedInImage() == nil){
                    let url = URL(string: "http://127.0.0.1:8000/assets/frontend/images/user/user-profile.png")
                    let data = try Data(contentsOf: url!)
                    profileImage.image = UIImage(data: data)
                }
                else{
                    let url = URL(string: "http://127.0.0.1:8000"+UserDefaults.standard.getLogedInImage())
                    let data = try Data(contentsOf: url!)
                    profileImage.image = UIImage(data: data)
                }
            }
            catch{
                print(error)
            }
            userName.text = UserDefaults.standard.getLogedInFirstName()+" "+UserDefaults.standard.getLogedInLastName()
        }
    }
    
    
    func logOut(){
        UserDefaults.standard.setIsLogedIn(value: false)
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "user_first_name")
        UserDefaults.standard.removeObject(forKey: "user_last_name")
        UserDefaults.standard.removeObject(forKey: "user_email")
        UserDefaults.standard.removeObject(forKey: "image")
        NotificationCenter.default.post(name: NSNotification.Name("HomeToLogin"), object: nil)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
        switch indexPath.row {
        case 0: NotificationCenter.default.post(name: NSNotification.Name("ShowProfile"), object: nil)
        case 1: NotificationCenter.default.post(name: NSNotification.Name("ShowNotification"), object: nil)
        case 2: logOut()
            
        default: break
        }
    }
  
    /*Reload side menu when opened*/
    @objc func reloadSlideMenu(notification: NSNotification){
        //do stuff
    
        if UserDefaults.standard.isLogedIn() == true{
            tableView.reloadData()
            do {
                if(UserDefaults.standard.getLogedInImage() == nil){
                    let url = URL(string: "http://127.0.0.1:8000/assets/frontend/images/user/user-profile.png")
                    let data = try Data(contentsOf: url!)
                    profileImage.image = UIImage(data: data)
                }
                else{
                    let url = URL(string: "http://127.0.0.1:8000"+UserDefaults.standard.getLogedInImage())
                    let data = try Data(contentsOf: url!)
                    profileImage.image = UIImage(data: data)
                }
            }
            catch{
                print(error)
            }
            userName.text = UserDefaults.standard.getLogedInFirstName()+" "+UserDefaults.standard.getLogedInLastName()
        }
    }
}
