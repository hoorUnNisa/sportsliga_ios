//
//  ForgotPasswordController.swift
//  Forgot pasword
///Users/zahid/Desktop/Forgot pasword/Forgot pasword/ForgotPasswordController.swift
//  Created by Zahid on 8/10/18.
//  Copyright © 2018 Zahid. All rights reserved.
//

import UIKit
import Alamofire
class ForgotPasswordController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var reques: UIButton!
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func reques(_ sender: Any) {
        var valid = true
        guard let Email = email.text, email.text?.count != 0
            else {
               
                let alert = UIAlertController(title: "Empty Email", message: "pleasw enter email.", preferredStyle: .alert)
              
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
                valid = false
                return
        }
    
        if isValidEmail(email: Email) == false {
            
            let alert = UIAlertController(title: "Empty Email", message: "pleasw enter valid email.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            valid = false
            return
            
        }
        if valid == true {
            ProgressHUD.show("Please wait")
        var forgotPassword : Dictionary = [String:String]()
          forgotPassword["email"] = self.email.text
        let url = "http://127.0.0.1:8000/api/resetPassword"
        Alamofire.request(url, method: .post, parameters: forgotPassword, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            let status = response.result.value as! [String:Any]
            let error = response.result.value as! [String:Any]
            ProgressHUD.dismiss()
            let errorstatus = error["erorr"]
            if(status["status"] != nil){
                let alert = UIAlertController(title: "Email", message: status["status"] as! String, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            else if(error["error"] != nil){
                let alert = UIAlertController(title: "Email", message: error["error"] as! String, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            }
        }
        
        }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        email?.self.borderStyle = .none
        email?.self.layer.backgroundColor = UIColor.white.cgColor
        
        email?.self.layer.masksToBounds = false
        email?.self.layer.shadowColor = UIColor.gray.cgColor
        email?.self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        email?.layer.shadowOpacity = 1.0
        email?.self.layer.shadowRadius = 0.0
        
        self.reques?.layer.borderColor = UIColor(red: 50/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        
//        self.reques?.layer.borderWidth = 2
        self.reques?.layer.cornerRadius = 25;
//        self.reques?.layer.backgroundColor = UIColor(red: 50/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
    }
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

