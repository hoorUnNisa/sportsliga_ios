//
//  MatchDetailViewController.swift
//  SportsLiga
//
//  Created by Ali Iqbal on 7/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class MatchDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    var getMatchId           = Int()
    var getFirstTeamImgUrl   = String()
    var getSecondteamImgUrl  = String()
    var getFirstTeamName     = String()
    var getSecondTeamName    = String()
    var getStartDate         = String()
    var getStartTime         = String()
    var CommentIdForReply    = Int()
    var comment_id:[Int]     = []
    var replyCommentId:[Int] = []
    var commentReplyText:[String] = []
    var matchComments : [Comment] = []
    var updateCommentText    = String()
    var getCommentId         = Int()
    var updateCommentView    = UIView()
    var textView             = UITextView()
    var likesUserId:[Int]    = []
    var likesCommentId:[Int] = []
    var against_user = String()
    
    @IBOutlet weak var likebutton: UIView!
    @IBOutlet weak var firstTeamScore: UILabel!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var commentArea: UITextField!
    @IBOutlet weak var startDateTime: UILabel!
    @IBOutlet weak var secondTeamName: UILabel!
    @IBOutlet weak var firstTeamName: UILabel!
    @IBOutlet weak var secondTeamScore: UILabel!
    @IBOutlet weak var firstTeamImg: UIImageView!
    @IBOutlet weak var secondTeamImg: UIImageView!
    @IBOutlet weak var MatchScoreTable: UITableView!
    @IBOutlet weak var deleteComment: UIButton!
    
    @IBAction func backTapButton(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }
    @IBAction func CommentReply(_ sender: Any) {
        if (UserDefaults.standard.isLogedIn()){
        print("hi")
         let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let CommentReply = storyBoard.instantiateViewController(withIdentifier: "CommentReplyC") as!CommentReplyController
//        CommentReply.against_user =
        self.CommentIdForReply = (sender as AnyObject).tag
        //self.against_user = (sender as AnyObject).accessibilityValue as! String
        CommentReply.comment_id = self.CommentIdForReply
        CommentReply.match_id = self.getMatchId
        CommentReply.user_id = UserDefaults.standard.getLogedInId()
        CommentReply.replytext = textView.text
        CommentReply.against_user = UserDefaults.standard.getLogedInFirstName()
         self.present(CommentReply, animated:true, completion:nil)
        }
        else{
          loginAlert()
        }

    }
    
    @IBAction func editComment(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.popView.isHidden = true
        let CommentUpateVC = storyBoard.instantiateViewController(withIdentifier: "CommentOptionVC") as! CommentClickViewController
        CommentUpateVC.commentText = self.updateCommentText
        CommentUpateVC.commentId   = self.getCommentId
        self.present(CommentUpateVC, animated:true, completion:nil)
    
    }
    @IBAction func closePopView(_ sender: UIButton) {
        ProgressHUD.show("wait...", interaction: false)
        self.popView.isHidden = true
        ProgressHUD.dismiss()
    }
    
    
    @IBAction func deleteComment(_ sender: UIButton) {
    
        ProgressHUD.show("Wait Deleting", interaction: false)
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.popView.isHidden = true
            
            self.MatchScoreTable.isUserInteractionEnabled = true
        })
        var commentArray : Dictionary<String, Any> = [:]
        commentArray["commentId"] = self.getCommentId
        let myUrl = URL(string: "http://127.0.0.1:8000/api/comment/delete");
        var request = URLRequest(url:myUrl!)
        request.httpMethod = "POST"// Compose a query string
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: commentArray, options: []) else { return }
        request.httpBody = httpbody
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            // print out response object
            //  print("response = \(response)")
            //Let's convert response sent from a server side script to a NSDictionary object:
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    if parseJSON["delete"] != nil{
                        ProgressHUD.dismiss()
                        self.showMatchCommentsApiCall()
                        self.alertDeleted()
                        return
                    }else {
                        if parseJSON["error"] != nil{
                            ProgressHUD.dismiss()
                            self.showMatchCommentsApiCall()
                            self.alertErrorDeletingComment()
                        }
                    }
                }
            } catch {
                print(error)
            }
        }
        task.resume()
    }
    //Like & Dislike Comments Api
    @IBAction func like(_ sender: Any) {
        if UserDefaults.standard.isLogedIn() {
            ProgressHUD.show("Wait...", interaction: false)
       var commentArray : Dictionary<String, Any> = [:]
            commentArray["userId"]     = UserDefaults.standard.getLogedInId()
            commentArray["commentId"] = (sender as AnyObject).tag
            
        let myUrl = URL(string: "http://127.0.0.1:8000/api/like");
        var request = URLRequest(url:myUrl!)
        
        request.httpMethod = "POST"// Compose a query string
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: commentArray, options: []) else { return }
        request.httpBody = httpbody
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            // print out response object
            // print("response = \(response)")
            // Let's convert response sent from a server side script to a NSDictionary object:
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if let parseJSON = json {
                    
                    if parseJSON["error"] != nil{
                        print ("Error!")
                    ProgressHUD.dismiss()
                    }
                    if parseJSON["like"] != nil{
                        DispatchQueue.main.async {
                            (sender as AnyObject).setImage(UIImage(named: "like fill"), for: .normal)

                        }
                      
                        ProgressHUD.dismiss()
                        print("liked")
                        
                    }
                    if parseJSON["dislike"] != nil{
                        DispatchQueue.main.async {
                            (sender as AnyObject).setImage(UIImage(named: "like"), for: .normal)

                        }
                        //self.showMatchCommentsApiCall()
                        ProgressHUD.dismiss()
                        print("dislike")
                    }
                }
            } catch {
                print(error)
            }
        }
        task.resume()
        }
    }
    public func ShowReplyCommentsCall(){
        matchComments = []
//        showMatchCommentsApiCall()
       // ProgressHUD.show("Please Wait", interaction: false)
        print(CommentIdForReply)
            let url  = "http://127.0.0.1:8000/api/getCommentReply/\(getMatchId)"
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: nil).responseArray{
                (response : DataResponse<[Comment]>) in
                print(response.result.value)
    
                if(response.response?.statusCode == 200){
                    self.matchComments.append(contentsOf: response.result.value!)
                    print(self.matchComments)
                    DispatchQueue.main.async {
                        self.MatchScoreTable.reloadData()
                    }
                }
            }
        }
    @IBAction func comment(_ sender: Any) {
        if commentArea.text == "" {
            commentArea.attributedPlaceholder = NSAttributedString(string: "Please Enter Your Comment!",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            
        }else{
            //create comment
            if UserDefaults.standard.isLogedIn() {
                var commentArray : Dictionary<String, Any> = [:]
                commentArray["userId"]    = UserDefaults.standard.getLogedInId()
                commentArray["matchId"]   = getMatchId
                commentArray["comments"]  = commentArea.text
                let myUrl = URL(string: "http://127.0.0.1:8000/api/createComment");
                commentArea.text = ""
                var request = URLRequest(url:myUrl!)
                
                request.httpMethod = "POST"// Compose a query string
                request.addValue("application/json", forHTTPHeaderField: "Content-type")
                guard let httpbody = try? JSONSerialization.data(withJSONObject: commentArray, options: []) else { return }
                request.httpBody = httpbody
                let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                    
                    if error != nil
                    {
                        print("error=\(String(describing: error))")
                        return
                    }
                    // print out response object
                    //  print("response = \(response)")
                    //Let's convert response sent from a server side script to a NSDictionary object:
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        if let parseJSON = json {
                            if parseJSON["user"] != nil{
                               self.showMatchCommentsApiCall()
                            }
                        }
                    } catch {
                        print(error)
                    }
                }
                task.resume()
            }else{
                commentArea.text = ""
               loginAlert()
                
    
            }
        }
    }
    //Login To Guest User when commenting on scoreboard.
    public func loginAlert(){
        let alertController = UIAlertController(title: "You are not logged In", message: "Please Login to comment.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "LogIn", style: .default) { (_) in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextViewController, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){ (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    //showing team scoreboard data.
    public func scoreBoardApiCall(){
        guard let url = URL(string: "http://127.0.0.1:8000/api/scoreboard/\(getMatchId)") else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    
                    print(error?.localizedDescription ?? "Response Error")
                    
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
                
                //print(jsonResponse) //Response result
                
                 guard let res = jsonResponse as? [[String: Any]] else { return }
                for score in res{
                if score["firstTeamScore"] != nil {
                    guard let firstTeamScore = score["firstTeamScore"] as? String else { return }
                    DispatchQueue.main.async {
                        self.firstTeamScore.text = firstTeamScore
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        self.firstTeamScore.text = "N/A"
                    }
                }
                if score["secondTeamScore"] != nil {
                    guard let secondTeamScore = score["secondTeamScore"] as? String else{ return }
                    DispatchQueue.main.async {
                        self.secondTeamScore.text = secondTeamScore
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        self.secondTeamScore.text = "N/A"
                    }
                }
                //                if res?["matchResult"]  != nil {
                //                guard let descrip = res!["matchResult"] as? String else{ return }
                //                }
                }
            }   catch let parsingError {
                print("Error", parsingError)
            }
            
        }
        task.resume()
        firstTeamName.text  = getFirstTeamName
        secondTeamName.text = getSecondTeamName
        startDateTime.text  = getStartDate + ". " + getStartTime
        let imageUrlString = "http://127.0.0.1:8000"+getFirstTeamImgUrl
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                firstTeamImg.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        let secondImgUrl = "http://127.0.0.1:8000"+getSecondteamImgUrl
        if let url = URL(string : secondImgUrl){
            do {
                let data = try Data(contentsOf: url)
                secondTeamImg.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        
    }
    public func showMatchCommentsApiCall(){
        matchComments = []
        ProgressHUD.show("Please Wait", interaction: false)
        let urlString = "http://127.0.0.1:8000/api/matchComments/\(getMatchId)"
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseArray { (response : DataResponse<[Comment]>) in
            //print(response.result.value as Any)
            self.matchComments.append(contentsOf: response.result.value!)
            DispatchQueue.main.async {
                self.MatchScoreTable.reloadData()
            }
            ProgressHUD.dismiss()
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popView.isHidden = true
        popView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        MatchScoreTable.delegate = self
        MatchScoreTable.dataSource = self
        MatchScoreTable.backgroundColor = UIColor.clear
        self.commentArea.layer.borderWidth = 1
        //self.commentField.layer.borderColor = UIColor.lightGray.cgColor
        MatchScoreTable.addSubview(refreshControl)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        scoreBoardApiCall()
    showMatchCommentsApiCall()
         ShowReplyCommentsCall()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.matchComments.count == 0{
            return 1
        }
        else{
        return self.matchComments.count
        }
    }
    // scroll replycomment agains related comments
    @objc func scrollToComment(sender: UIButton){
        let commentId = sender.tag
        var indexPath:Int?
        for i in 0..<matchComments.count {
            if(commentId == matchComments[i].commentId && matchComments[i].reply_id == nil){
                indexPath = i
            }
        }
        let pathToLastRow = IndexPath.init(row: indexPath!, section: 0)
         MatchScoreTable.scrollToRow(at: pathToLastRow, at: .none, animated: true)
    }
    
    // table view for rowIndex
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchScoreCell", for: indexPath) as! MatchScoreTableViewCell
        
    if self.matchComments.count == 0{
            cell.userName.text     = "No Comment on this Match."
            cell.likeBtn.isHidden  = true
            cell.Replybtn.isHidden = true
            cell.Replybtn.isHidden = true
            cell.userImg.isHidden  = true
            cell.comment.isHidden  = true
            cell.replyOf.isHidden  = true
            cell.layer.borderWidth = 0.5
        }
        else{
        cell.likeBtn.isHidden   = false
        cell.Replybtn.isHidden  = false
        cell.Replybtn.isHidden  = false
        cell.userImg.isHidden   = false
        cell.comment.isHidden   = false
        cell.replyOf.isHidden = true
        cell.layer.borderWidth  = 0.5
        cell.userName.text      = matchComments[indexPath.row].commentsUserName
        cell.commentId          = matchComments[indexPath.row].commentId
        cell.userId             = matchComments[indexPath.row].commmentsUserId
        cell.comment.text       = matchComments[indexPath.row].comments
        cell.Replybtn.tag       = matchComments[indexPath.row].commentId!
        cell.comment.isEditable = false
        cell.likeBtn.tag = matchComments[indexPath.row].commentId!
           // working of replycomment scrolling in cell
            if(matchComments[indexPath.row].reply_id != nil){
                cell.likeBtn.isHidden = true
                cell.Replybtn.isHidden = true
                cell.replyOf.isHidden = false
                let btnTitle = "Reply of "+matchComments[indexPath.row].against_user!+" comment"
                cell.replyOf.setTitle(btnTitle, for: .normal)
                cell.replyOf.tag = matchComments[indexPath.row].commentId!
                cell.replyOf.addTarget(self, action: #selector(scrollToComment(sender:)), for: .touchUpInside)
            }
        //show user likes if user is logged in & had liked comments.
        if UserDefaults.standard.isLogedIn(){
            let userID = UserDefaults.standard.getLogedInId()
            if userID == matchComments[indexPath.row].likesUserId && matchComments[indexPath.row].commentId == matchComments[indexPath.row].likesCommentId{
                cell.likeBtn.setImage(UIImage(named: "like fill"), for: .normal)
            }else{
                cell.likeBtn.setImage(UIImage(named: "like"), for: .normal)
            }
        }

        let imgUrl = matchComments[indexPath.row].commentsUserImgUrl as String?
        if imgUrl == "nil" {
            cell.userImg.image = UIImage(named: "noimage")
        }else{
            let imageUrlString = "http://127.0.0.1:8000"+matchComments[indexPath.row].commentsUserImgUrl!
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
             cell.userImg.image = UIImage(data: data)

            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        }
//        cell.Replybtn.tag    = matchComments[indexPath.row].commentId!
//        cell.comment.text    = matchComments[indexPath.row].comments
//        cell.Replybtn.accessibilityValue = matchComments[indexPath.row].commentsUserName
        }
        return cell
        
    }
    // show when comment is successfully deleted.
    public func alertDeleted(){
        let alertController = UIAlertController(title: "Comment Deleted:", message: "Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
           
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
   //showed when some error occured while deleting comment
    public func alertErrorDeletingComment(){
        let alertController = UIAlertController(title: "Error!", message: "Error Deleting Comment. Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
  
    
   
    
    //show option of delete & edit comment
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = tableView.dequeueReusableCell(withIdentifier: "MatchScoreCell", for: indexPath) as! MatchScoreTableViewCell
        if(matchComments[indexPath.row].reply_id != nil){
            self.popView.isHidden = true
        }
        else{
        if UserDefaults.standard.isLogedIn() && matchComments.count != 0{
            
            let userID = UserDefaults.standard.getLogedInId()
            if userID == self.matchComments[indexPath.row].commmentsUserId {
                     self.getCommentId      = self.matchComments[indexPath.row].commentId!
                     self.updateCommentText = self.matchComments[indexPath.row].comments!
                UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                   
                self.popView.isHidden = false
                })
                
            }
            
        }
        else{
            return
        }
        }
        
    }
    // refresh comments
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MatchDetailViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        showMatchCommentsApiCall()
        
        refreshControl.endRefreshing()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}




