//
//  StadiumVC.swift
//  SportLigaNew
//
//  Created by hidayatullah on 09/08/2018.
//  Copyright © 2018 Hidayat Ullah. All rights reserved.
//

import UIKit
import SVProgressHUD
class StadiumVC: UIViewController {
    var getTeamID = Int()
    var stadiumImgUrl = String()
    @IBOutlet weak var stdDescription: UITextView!
    @IBOutlet weak var stadiumImg: UIImageView!
    @IBOutlet weak var stadiumName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Stadium"
        // Do any additional setup after loading the view.
    }
    public func stadiumApiCall(){//fetch team stadium details Api
        let tabBarC = tabBarController as! TeamTabBarViewController
        getTeamID = tabBarC.getTeamId
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
        guard let url = URL(string: "http://127.0.0.1:8000/api/stadium/\(getTeamID)") else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    
                    print(error?.localizedDescription ?? "Response Error")
                    
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
                //print(jsonResponse) //Response result
                guard let res = jsonResponse as? [[String: Any]] else {return }
    
                for res in res{
                guard let name         = res["stdName"] as? String else { return }
                    self.stadiumImgUrl = (res["stdImg"] as? String)!
                    guard let descrip  = res["description"] as? String else{ return }
                DispatchQueue.main.async {
                    if name == "" {
                        self.stadiumName.text = "N/A"
                    }else{
                        self.stadiumName.text = name
                    };if descrip == "" {
                        self.stdDescription.text = "No Description"
                    }else{
                        self.stdDescription.text = descrip
                    }
                    
                    self.stdDescription.isEditable = false
                }
                
                }
                if self.stadiumImgUrl != " "{
                let imageUrlString = "http://127.0.0.1:8000" + self.stadiumImgUrl
                if let url = URL(string : imageUrlString){
                    do {
                        let data = try Data(contentsOf: url)
                        DispatchQueue.main.async {
                        self.stadiumImg.image = UIImage(data: data)
                        }
                       
                        
                    }catch let err {
                        print(" Error : \(err.localizedDescription)")
                    }
                }
                }
                else{
                    DispatchQueue.main.async {
                        self.stadiumImg.image = UIImage(named: "stadium-1")
                    }
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
            
        }
        
        task.resume()
     
      
    }
        
    
    override func viewWillAppear(_ animated: Bool) {
        stadiumApiCall()
        SVProgressHUD.dismiss()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
