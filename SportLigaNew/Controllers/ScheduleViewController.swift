//
//  ScheduleViewController.swift
//  SportLigaNew
//
//  Created by Tahir on 10/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

class ScheduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataImg: UIImageView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var filterBtn: UIButton!
    @IBAction func filterButtonAction(_ sender: Any) {
    }
    var result = String()
    var match: [Schedule] = []
    var url = String()
    var first = String()
    var last = String()
    var isInProgress : Bool = true
    var timer = Timer()
    var page = 0
    var todayDate = String()
    var getTournamentId = Int()
    // first date in the range
    private var firstDate: Date?
    // last date in the range
    private var lastDate: Date?
    private var datesRange: [Date]?
    @IBOutlet weak var scheduleTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        noDataView.isHidden = true
        // setting table delegates and sources
        scheduleTbl.delegate = self
        scheduleTbl.dataSource = self
        scheduleTbl.addSubview(scheduleFresher)
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)

//        self.view.addSubview(self.)
        self.todayDate = self.curruntDate()
        
    }
    
    @objc func loadList(notification: NSNotification){
        SVProgressHUD.show(withStatus: "Please Wait")
        self.match.removeAll()
        self.page = 0
        DispatchQueue.main.async {
            self.url = "http://127.0.0.1:8000/api/fetchMatches"
            self.fetchSchedule(url: self.url)
        }
    }
    //calling function to fetch data from api + loader
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Schedule"
        SVProgressHUD.show(withStatus: "Please Wait")
        
        //        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ScheduleTableViewController.fetchSchedule), userInfo: nil, repeats: true)
        DispatchQueue.main.async {
            self.url = "http://127.0.0.1:8000/api/fetchMatches"
            self.fetchSchedule(url: self.url)
        }
    }
    
    //checking search date is valid
    func curruntDate() -> String {
        let startWeek = Date()
//        print(startWeek!)
        let dateFormat1 = DateFormatter()
        dateFormat1.dateFormat = "yyyy-MM-dd"
        let startWeek2 = dateFormat1.string(from: startWeek)
        print(startWeek2)
        return startWeek2
        
    }
    

    //data fetching from api
    func fetchSchedule(url: String){
        //let Url = "http://127.0.0.1:8000/api/fetchMatches"
        var matchArray : Dictionary = [String:String]()
        let getData =  tabBarController as! TournamentTabBarViewController
        getTournamentId = getData.getTournamentId
        print(getTournamentId)
        matchArray["tournament_id"] = "\(getTournamentId)"
        matchArray["page"] = "\(page)"
        if(UserDefaults.standard.getStartDate().isEmpty == false){
            matchArray["start_date"] = UserDefaults.standard.getStartDate()
        }
        else{
            matchArray["start_date"] = self.todayDate
            UserDefaults.standard.setStartDate(start_date: self.todayDate)
        }
        if(UserDefaults.standard.getEndDate().isEmpty == false){
            matchArray["end_date"] = UserDefaults.standard.getEndDate()
        }
        else{
            matchArray["end_date"] = ""
        }
        if(UserDefaults.standard.getTeam().isEmpty == false){
            matchArray["team"] = UserDefaults.standard.getTeam()
        }
        else{
            matchArray["team"] = ""
        }
        print(matchArray)
        Alamofire.request(url, method: .post, parameters: matchArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Schedule]>) in
            if(response.response?.statusCode == 404)  {
                SVProgressHUD.dismiss()
                if(self.match.isEmpty){
                    self.noDataView?.isHidden = false
                    self.scheduleTbl.isHidden = true
                    self.alert(title: "Alert", message: "Result not Found!")
                }
                    self.isInProgress = false
//                    self.scheduleTbl.isHidden = true
//                    self.alert(title: "Alert", message: "Result not Found!")
            }
            if(response.response?.statusCode == 200) {
                if(response.result.value?.isEmpty)! == false{
                //if(self.scheduleTbl.isHidden == true){
                    self.scheduleTbl.isHidden = false
                    self.isInProgress = true
                    self.match.append(contentsOf: response.result.value!)
                    DispatchQueue.main.async {
                        self.scheduleTbl.reloadData()
                        if self.match.count == 0{
                            self.noDataView.isHidden = false
                        }
                        else{
                        self.noDataView?.isHidden = true
                        }
                    }
                    
                if ((response.result.value?.isEmpty)! && self.page > 0)
                {
                    self.isInProgress = false
                }
                SVProgressHUD.dismiss()
            }
            }
        }
        
    }
    //function to call fetchSchedule to parse data from api
    @objc func fetchData(){
        self.fetchSchedule(url: url)
        SVProgressHUD.dismiss()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    //return total count of match/ schedule
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of row
        return  match.count
        
        }
    
        // Do any additional setup after loading the view.
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Schedule1Cell", for: indexPath) as! Schedule1TableViewCell
        
        
        //team one image
            let imageUrlString = "http://127.0.0.1:8000"+match[indexPath.row].team_one_image!
            if let url = URL(string : imageUrlString){
                do {
                    let data = try Data(contentsOf: url)
                    cell.FirstTeamLogo.image = UIImage(data: data)
                    
                }catch let err {
                    print(" Error : \(err.localizedDescription)")
                }
            }
            //team two image
            let imageUrlString1 = "http://127.0.0.1:8000"+match[indexPath.row].team_two_image!
            
            if let url = URL(string : imageUrlString1){
                do {
                    let data = try Data(contentsOf: url)
                    cell.SecondTeamLogo.image = UIImage(data: data)
                    
                }catch let err {
                    print(" Error : \(err.localizedDescription)")
                }
            }
            cell.FirstTeamLabel.text = match[indexPath.row].first_team_name
            cell.TeamOneGoal.text = match[indexPath.row].team_one_score
            cell.TeamTwoGoal.text = match[indexPath.row].team_two_score
            cell.MatchDate.text = match[indexPath.row].date
            cell.SecondTeamLabel.text = match[indexPath.row].second_team_name
            cell.Location.text = match[indexPath.row].start_time
            cell.matchId.text = "\(match[indexPath.row].id)"
            cell.matchId.isHidden = true
        
            return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //sending data on match details
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DvC = storyBoard.instantiateViewController(withIdentifier: "MatchDetailViewController") as! MatchDetailViewController
        //        let id:Int = Int(self.teams[indexPath.row].id)
        DvC.getMatchId = (match[indexPath.row].id)!
        DvC.getFirstTeamImgUrl = (match[indexPath.row].team_one_image)!
        DvC.getSecondteamImgUrl = match[indexPath.row].team_two_image!
        DvC.getFirstTeamName = match[indexPath.row].first_team_name!
        DvC.getSecondTeamName = match[indexPath.row].second_team_name!
        DvC.getStartTime = match[indexPath.row].start_time!
        DvC.getStartDate = match[indexPath.row].date!
        self.navigationController?.pushViewController(DvC, animated: true)
    }

    //pagination in schedule
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == match.count - 1 {
            if isInProgress {
                print("continue fetching")
                self.page = self.page + 1
                self.perform(#selector(self.fetchData), with: nil, afterDelay: 3)
                //spinner at last row of page
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.scheduleTbl.tableFooterView = spinner
                self.scheduleTbl.tableFooterView?.isHidden = false
            }
            
        }
    }
    // schadule alert for wrong search
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //pull more function
    lazy var scheduleFresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ScheduleViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.blue
        
        return refreshControl
    }()
    //pull to refresh
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        fetchSchedule(url: "http://127.0.0.1:8000/api/fetchMatches")
        refreshControl.endRefreshing()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension Date {
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
}
