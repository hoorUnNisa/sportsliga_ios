//
//  ViewController.swift
//  TeamApiScreen
//
//  Created by hidayatullah on 26/07/2018.
//  Copyright © 2018 hidayatullah. All rights reserved.
//

import UIKit
import SVProgressHUD
class TeamDetailViewController: UIViewController {
    var getId = Int()
    var getImageUrl = String()
    
    
    @IBOutlet weak var descrip: UITextView!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var country: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
      
      image.clipsToBounds = true
        //image.layer.cornerRadius = image.frame.height/2
        image.center = view.center
    }
    public func teamDetailApiCall(){// fetching team details from Api
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
        guard let url = URL(string: "http://127.0.0.1:8000/api/team/\(getId)") else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    
                    print(error?.localizedDescription ?? "Response Error")
                    
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
                
                //print(jsonResponse) //Response result
                
                let res = jsonResponse as? [String: Any]
                guard let name    = res!["name"] as? String else { return }
                guard let country = res!["country"] as? String else{ return }
                guard let descrip = res!["description"] as? String else{ return }
                DispatchQueue.main.async {
                    if name == "" {
                        self.name.text = "N/A"
                    }else{
                        self.name.text = name
                    };if country == ""{
                        self.country.text = "N/A"
                    }else{
                        self.country.text = country
                    };if descrip == "" {
                        self.descrip.text = "No Description"
                    }else{
                        self.descrip.text = descrip
                    }
                    
                    self.descrip.isEditable = false
                }
                
                
                
            } catch let parsingError {
                print("Error", parsingError)
            }
            SVProgressHUD.dismiss()
        }
        task.resume()
        let imageUrlString = "http://127.0.0.1:8000"+getImageUrl
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                image.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        teamDetailApiCall()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


