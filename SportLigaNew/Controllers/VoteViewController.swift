//
//  VoteViewController.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 9/25/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
class VoteViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var pollResult: UILabel!
    @IBOutlet weak var pollTitle: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var pollEndTime: UILabel!
    var timeEnd : Bool?
    var poll_id = Int()
    var poll : PollDetail?
    var btnDisable  = false
    var playerCount : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        table.delegate = self
        table.dataSource = self
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
        pollResult.isHidden = true
    }
    
    /* fetch single poll data and parse it */
    
    func fetchData(){
        
        let urlString = "http://127.0.0.1:8000/api/poll"
        
        /* if user is login the send user id for check user vote */
        
        if(UserDefaults.standard.isLogedIn() == true){
            SVProgressHUD.show(withStatus: "Please Wait")
            let user_id = UserDefaults.standard.getLogedInId()
            var userArray : Dictionary = [String: Any]()
            userArray["user_id"] = user_id
            userArray["id"] = poll_id
            print("poll_id")
            print(poll_id)
            Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                (response : DataResponse<PollDetail>) in
                if(response.response?.statusCode == 200){
                    self.poll = response.result.value
                    self.playerCount = (self.poll?.poll_competitors.count)!
                    self.showResult()
                    //print(currentDate)
                    self.pollEndTime.text = "End Date "+(self.poll?.poll_end_date)!
                    // self.pollEndDate.text = "Voting Open Till "+(self.poll?.poll_end_date)!
                    DispatchQueue.main.async {
                        self.table.reloadData()
                    }
                    self.showResult()
                    self.pollTitle.text = self.poll?.poll_title
                }
                SVProgressHUD.dismiss()
            }
            
        }else{
            
            /* if user is not login fetch poll detial */
            
            ProgressHUD.show("Please Wait", interaction: false)
            var userArray : Dictionary = [String: Any]()
            userArray["id"] = poll_id
            Alamofire.request(urlString, method: .post,parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                (response : DataResponse<PollDetail>) in
                if(response.response?.statusCode == 200){
                    self.poll = response.result.value
                    self.playerCount = (self.poll?.poll_competitors.count)!
                    self.pollEndTime.text = "End Date "+(self.poll?.poll_end_date)!
                    DispatchQueue.main.async {
                        self.table.reloadData()
                    }
                    self.showResult()
                }
                 ProgressHUD.dismiss()
                
            }
           
        }
        
    }
    
    /* show poll result after time out */
    
    func showResult(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let endDate = dateFormatter.date(from: (self.poll?.poll_end_date!)!)
        let currentDate = Date()
        if(currentDate > endDate!){
            pollResult.isHidden = false
            timeEnd = true
            if(poll?.winner_index! != -1){
                pollResult.text = (poll?.poll_competitors[(poll?.winner_index!)!].competitor_name)!+" win the poll"
            }else{
                pollResult.text = "Poll Tie"
            }
        }else{
            pollResult.text = " "
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("number of count")
        return self.playerCount
        
    }
    
    /* show poll user in cell */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var competitorImage:String?
        let cell = tableView.dequeueReusableCell(withIdentifier: "VotingPollCell", for: indexPath) as! VotingPollTableViewCell
        cell.selectionStyle = .none
        cell.name.text = self.poll?.poll_competitors[indexPath.row].competitor_name
        cell.totalVotes.text = String(describing: (self.poll?.poll_competitors[indexPath.row].competitor_votes)!)
        if(poll?.poll_competitors[indexPath.row].competitor_type! == "team"){
            competitorImage = "http://127.0.0.1:8000"+(poll?.poll_competitors[indexPath.row].competitor_image!)!
        }else{
            competitorImage = poll?.poll_competitors[indexPath.row].competitor_image!
        }
        
        cell.teamName.text = poll?.poll_competitors[indexPath.row].competitor_name!
        if let url = URL(string : competitorImage!){
            do {
                let data = try Data(contentsOf: url)
                cell.playerImage.image = UIImage(data: data)
                print(data)
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        
        /* If user is login check user vote */
        
        if(UserDefaults.standard.isLogedIn() == true){
            if(poll?.user_vote != nil || timeEnd == true){
                cell.voteButton.addTarget(self, action: #selector(voteAlreadyCast), for: .touchUpInside)
                if(poll?.poll_competitors[indexPath.row].competitor_id == poll?.user_vote?.competitor_id){
                    cell.voteButton.isEnabled = false
                    cell.voteButton.setTitle("Voted", for: .normal)
                }
                cell.voteButton.backgroundColor = UIColor.gray
                cell.voteButton.isEnabled = false
            }else{
                
                /* if user is not login then add user vote */
                
                cell.voteButton.tag =  (poll?.poll_competitors[indexPath.row].competitor_id)!
                cell.voteButton.addTarget(self, action: #selector(addVote(sender:)), for: .touchUpInside)
                
            }
        }else{
            cell.voteButton.addTarget(self, action: #selector(showLoginAlert), for: .touchUpInside)
        }
        return cell
    }
    
    /* add vote after click on vote button */
    
    @objc func addVote(sender: UIButton) {
        print("vote cast")
        let urlString = "http://127.0.0.1:8000/api/addVote"
        var userArray : Dictionary = [String: Any]()
        userArray["competitor_id"] = sender.tag
        userArray["poll_id"] = (poll?.poll_id)!
        userArray["user_id"] = UserDefaults.standard.getLogedInId()
        print(userArray)
        Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
            (response : DataResponse<UserVote>) in
            if(response.response?.statusCode == 200){
                self.fetchData()
            }
        }
    }
    
    /* if user is not login show alert  */
    
    @objc func showLoginAlert(){
        let alertController = UIAlertController(title: "You are not logged In", message: "Please Login to comment.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "LogIn", style: .default) { (_) in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextViewController, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){ (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    /* if user is already caste their vote show alert */
    
    @objc func voteAlreadyCast(){
        let alert = UIAlertController(title: "Vote Casted", message: "You Already Cast Your Vote", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

