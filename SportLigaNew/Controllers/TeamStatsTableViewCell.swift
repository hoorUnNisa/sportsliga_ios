//
//  TeamStatsTableViewCell.swift
//  TeamStausScreen
//
//  Created by hidayatullah on 19/07/2018.
//  Copyright © 2018 hidayatullah. All rights reserved.
//

import UIKit

class TeamStatsTableViewCell: UITableViewCell {

    @IBOutlet weak var stats: UILabel!
    @IBOutlet weak var teamImg: UIImageView!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
