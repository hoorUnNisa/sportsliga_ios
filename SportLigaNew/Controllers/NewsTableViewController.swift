//
//  NewsTableViewController.swift
//  SportsLiga
//
//  Created by Admin on 26/09/1439 AH.
//  Copyright © 1439 Admin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

class NewsTableViewController: UITableViewController {
    var news: [News] = []
    var url = String()
    var isInProgress : Bool = true
    var timer = Timer()
    var page = 0
    @IBOutlet var newsTbl: UITableView!
    //pull more function
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var noDataImg: UIImageView!
    lazy var scheduleFresher : UIRefreshControl = {
        let fresherController = UIRefreshControl()
        fresherController.addTarget(self, action: #selector(refreshRequest), for: .valueChanged)
        return fresherController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        noDataImg.image            = UIImage(named: "nodata")
        noDataLabel.text           = "No News Available! Try Checking Later."
        noDataView.isHidden = true
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "News"
        SVProgressHUD.show(withStatus: "Please Wait")
        //        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ScheduleTableViewController.fetchSchedule), userInfo: nil, repeats: true)
        DispatchQueue.main.async {
            self.url = "http://127.0.0.1:8000/api/news"
            self.fetchNews(url: self.url)
        }
//        if news.count == 0{
//            noDataView.isHidden = false
//        }
//        else{
//            noDataView.isHidden = true
//        }
        SVProgressHUD.dismiss()
    }
    func fetchNews(url: String){
        //let Url = "http://127.0.0.1:8000/api/news"
        var matchArray : Dictionary = [String:String]()
        matchArray["page"] = "\(page)"
        Alamofire.request(url, method: .post, parameters: matchArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[News]>) in
            print(response.response?.statusCode)
            if(response.response?.statusCode == 404)  {
                self.isInProgress = false
                self.newsTbl.isHidden = true
                SVProgressHUD.dismiss()
                self.alert(title: "Alert", message: "Result not Found! ")
            }
            //            else{
            if(response.response?.statusCode == 200) {
                if(response.result.value?.isEmpty)! == false{
                    //if(self.scheduleTbl.isHidden == true){
                    self.newsTbl.isHidden = false
                    self.isInProgress = true
                    self.news.append(contentsOf: response.result.value!)
                    DispatchQueue.main.async {
                        self.newsTbl.reloadData()
                    }
                    if ((response.result.value?.isEmpty)! && self.page > 0)
                    {
                        self.isInProgress = false
                        self.newsTbl.tableFooterView?.isHidden = true
                    }
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }
    //function to call fetchSchedule to parse data from api
    @objc func fetchData(){
        self.fetchNews(url: url)
    }
    //function to pull more
    @objc func refreshRequest(){
        fetchNews(url: "http://127.0.0.1:8000/api/news")
        scheduleFresher.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return news.count
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsTableViewCell
       
        
          
        let imageUrlString1 = "http://127.0.0.1:8000"+news[indexPath.row].image!
        //news image
        if let url = URL(string : imageUrlString1){
            do {
                let data = try Data(contentsOf: url)
                cell.newsImage.image = UIImage(data: data)
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        cell.newsDateLabel.text = news[indexPath.row].publishedAt
        cell.newLabel.text = news[indexPath.row].description
        cell.newsHeading.text = news[indexPath.row].title
        
        return cell
    }
    //pagination in news
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == news.count - 1 {
            if isInProgress {
                print("continue fetching")
                self.page = self.page + 1
                self.perform(#selector(self.fetchData), with: nil, afterDelay: 3)
                //spinner at last row of page
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.newsTbl.tableFooterView = spinner
                self.newsTbl.tableFooterView?.isHidden = false
            }
            
        }
    }
    //sanding data to match details
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let result = storyBoard.instantiateViewController(withIdentifier: "NewsDetailViewController") as! NewsDetailViewController
        result.getNewsid = news[indexPath.row].id!
        result.getImage = news[indexPath.row].image!
        self.navigationController?.pushViewController(result, animated: true)
    }
    
    // news alert
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
