//
//  NewsDetailViewController.swift
//  SportLigaNew
//
//  Created by Tahir on 10/13/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

class NewsDetailViewController: UIViewController {
    var getNewsid = Int()
    var getImage = String()
    var news: [News] = []
    var url = String()
    @IBOutlet weak var newsDescLabel: UITextView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(getNewsid)
        self.navigationController?.navigationBar.topItem?.title = "News Detail"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show(withStatus: "Please Wait")
        if(getNewsid > 0){
            DispatchQueue.main.async {
                self.url = "http://127.0.0.1:8000/api/newsDetails"
                self.fetchNews(url: self.url)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func fetchNews(url: String){
        //let Url = "http://127.0.0.1:8000/api/newsDetails"
        var matchArray : Dictionary = [String:Int]()
        matchArray["id"] = getNewsid
        Alamofire.request(url, method: .post, parameters: matchArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[News]>) in
            if(response.response?.statusCode == 404)  {
                SVProgressHUD.dismiss()
                //self.alert(title: "Alert", message: "Result not Found!")
            }
            //            else{
            if(response.response?.statusCode == 200) {
                self.newsTitleLabel.text = response.result.value?[0].title
                self.newsDescLabel.text = response.result.value?[0].content
                do {
                    let url = URL(string: "http://127.0.0.1:8000"+self.getImage)
                    let data = try Data(contentsOf: url!)
                    self.newsImage.image = UIImage(data: data)
                }
                catch{
                    print(error)
                }
                SVProgressHUD.dismiss()
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
