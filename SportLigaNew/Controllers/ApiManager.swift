//
//  ApiManager.swift
//  SportLigaNew
//
//  Created by Tahir on 8/17/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class APIManager: NSObject {
    
    class func apiMultipart(serviceName:String,parameters: [String:Any]?, completionHandler: @escaping (JSON?, NSError?) -> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData:MultipartFormData) in
            for (key, value) in parameters! {
                if key == "imageName" {
                    //                    print(value)
                    multipartFormData.append(
                        value as! Data,
                        withName: key,
                        fileName: "image.jpeg",
                        mimeType: "image/jpeg"
                    )
                } else {
                    //Data other than image
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
            }
        }, usingThreshold: 1, to: serviceName, method: .post, headers: ["Content-Type":"application/json"]) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if response.result.error != nil {
                        completionHandler(nil,response.result.error as NSError?)
                        return
                    }
                    print(response.result.value!)
                    //UserDefaults.standard.setLogedInImage(image: response.result.value! as! String)
                    if let data = response.result.value {
                        let json = JSON(data)
                        print(json["result"])
                        completionHandler(json,nil)
                    }
                }
                break
                
            case .failure(let encodingError):
                print(encodingError)
                completionHandler(nil,encodingError as NSError?)
                break
            }
        }
    }
    
}
