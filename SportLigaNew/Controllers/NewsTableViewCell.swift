//
//  NewsTableViewCell.swift
//  SportsLiga
//
//  Created by Admin on 26/09/1439 AH.
//  Copyright © 1439 Admin. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsHeading: UILabel!
    @IBOutlet weak var newsDateLabel: UILabel!
    @IBOutlet weak var newLabel: UITextView!
    @IBOutlet weak var newsImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
