//
//  LiveScoreViewController.swift
//  SportLigaNew
//
//  Created by Admin on 14/01/1440 AH.
//  Copyright © 1440 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class LiveScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var noScoreLabel: UILabel!
    
    @IBOutlet weak var liveScoreTable: UITableView!
    @IBOutlet weak var noLive: UILabel!
    @IBOutlet weak var firstTeamGoal: UILabel!
    @IBOutlet weak var firstTeamName: UILabel!
    @IBOutlet weak var firstTeamImage: UIImageView!
    @IBOutlet weak var secondTeamGoal: UILabel!
    @IBOutlet weak var secondTeamImage: UIImageView!
    @IBOutlet weak var secondTeamName: UILabel!
    @IBOutlet weak var time: UILabel!
    var liveScore : LiveScore?
    var scoreCount : Int = 0
    var awayTeamId : String?
    var homeTeamId : String?
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        liveScoreTable.delegate = self
        liveScoreTable.dataSource = self
        noScoreLabel.isHidden = true
        // Do any additional setup after loading the view.
        noLive.isHidden = true;
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Live Score"
        self.firstTeamGoal.isHidden = true
        self.firstTeamName.isHidden = true
        self.firstTeamImage.isHidden = true
        self.secondTeamGoal.isHidden = true
        self.secondTeamImage.isHidden = true
        self.secondTeamName.isHidden = true
        self.time.isHidden = true
        /* Time for call api after 2 min */
        
        self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(LiveScoreViewController.fetchLiveScore), userInfo: nil, repeats: true)
        DispatchQueue.main.async{
           self.fetchLiveScore()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        /* dismiss time when view will disappear */
        
        timer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* Fetch single live score and parse it */
    
    @objc func fetchLiveScore(){
        ProgressHUD.show()
        let url = "http://127.0.0.1:8000/api/live-score"
        var userArray : Dictionary = [String: Any]()
        userArray["homeTeamId"] = homeTeamId!
        userArray["awayTeamId"] = awayTeamId!
        print(userArray)
        Alamofire.request(url, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
            (response : DataResponse<LiveScore>) in
            
            /* if status code is 200 */
            
            if(response.response?.statusCode == 200){
                print(response.result.value)
                self.liveScore = response.result.value
                DispatchQueue.main.async {
                    self.liveScoreTable.reloadData()
                }
                let firstTeamImageUrlString = "http://127.0.0.1:8000"+self.liveScore!.firstTeamImage!
                let secondTeamImageUrlString = "http://127.0.0.1:8000"+self.liveScore!.secondTeamImage!
                print(secondTeamImageUrlString)
                if let url = URL(string : firstTeamImageUrlString){
                    do {
                        let data = try Data(contentsOf: url)
                        self.firstTeamImage.image = UIImage(data: data)
                    }catch let err {
                        print(" Error : \(err.localizedDescription)")
                    }
                }
                if let url = URL(string : secondTeamImageUrlString){
                    do {
                        let data = try Data(contentsOf: url)
                        self.secondTeamImage.image = UIImage(data: data)
                    }catch let err {
                        print(" Error : \(err.localizedDescription)")
                    }
                }
                self.scoreCount = (self.liveScore?.score?.count)!
                if(self.scoreCount == 0){
                    self.noScoreLabel.isHidden = false
                    self.liveScoreTable.isHidden = true
                }else{
                    self.noScoreLabel.isHidden = true
                    self.liveScoreTable.isHidden = false
                }
                self.firstTeamName.text = self.liveScore!.firstTeamName
                self.firstTeamGoal.text = self.liveScore!.firstTeamGoals
                self.secondTeamName.text = self.liveScore!.secondTeamName
                self.secondTeamGoal.text = self.liveScore!.secondTeamGoals
                self.time.text =  self.liveScore!.time
                self.firstTeamGoal.isHidden = false
                self.firstTeamName.isHidden = false
                self.firstTeamImage.isHidden = false
                self.secondTeamGoal.isHidden = false
                self.secondTeamImage.isHidden = false
                self.secondTeamName.isHidden = false
                self.time.isHidden = false
                
            }
            if(response.response?.statusCode == 404){
                self.noLive.isHidden = false
                self.firstTeamName.isHidden = true
                self.firstTeamImage.isHidden = true
                self.firstTeamGoal.isHidden = true
                self.time.isHidden = true
                self.secondTeamName.isHidden = true
                self.secondTeamImage.isHidden = true
                self.secondTeamGoal.isHidden = true
            }
            
            ProgressHUD.dismiss()
        }
        
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreCount
    }
    
    /* add data to each cell*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveScoreTableViewCell", for: indexPath) as! LiveScoreTableViewCell
        print(liveScore?.score![indexPath.row].score_team)
        if(liveScore?.score![indexPath.row].score_team == "home"){
            cell.secondTeamScoreTime.isHidden = true
            cell.firstTeamScoreTime.text = liveScore?.score![indexPath.row].score_time
            cell.secondTeamPlayerName.isHidden = true
            cell.firstTeamPlayerName.text = liveScore?.score![indexPath.row].score_player_name
            
        }
        if(liveScore?.score![indexPath.row].score_team == "away"){
            cell.firstTeamPlayerName.isHidden = true
            cell.firstTeamScoreTime.isHidden = true
            cell.secondTeamScoreTime.text = liveScore?.score![indexPath.row].score_time
            cell.secondTeamPlayerName.text = liveScore?.score![indexPath.row].score_player_name
        }
        
        if(liveScore?.score![indexPath.row].score_type == "yellowCard"){
            cell.scoreType.image = UIImage(named: "yellow-card")
        }
        if(liveScore?.score![indexPath.row].score_type == "redCard"){
            cell.scoreType.image = UIImage(named: "red-card")
        }
        if(liveScore?.score![indexPath.row].score_type == "goal"){
            cell.scoreType.image = UIImage(named: "football")
        }
        
        print(liveScore?.score![indexPath.row].score_team)
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
