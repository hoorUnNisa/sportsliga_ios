//
//  TopPlayerTableViewCell.swift
//  SportLigaNew
//
//  Created by Hoor on 02/10/2018.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var pollLastDate: UILabel!
    @IBOutlet var PlayerOfPoll: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
