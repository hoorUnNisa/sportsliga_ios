//
//  ViewController.swift
//  standings_sportsliga
//
//  Created by HoorPC on 17/07/2018.
//  Copyright © 2018 HoorPC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
@available(iOS 11.0, *)
class StandingViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var StandingTitle: UILabel!
    var getTournamentId = Int()
    var getTourId = Int()
    var getTourName = String()
    var isInprogress : Bool = true
    var standings: [Standing] = []
    var timer = Timer()
    var page = 0
    //setting values to each cell elements to show on screen
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? StandingTableCell else{
            return UITableViewCell()
        }
        let imageUrlString = "http://127.0.0.1:8000"+standings[indexPath.row].Image!
        print(imageUrlString)
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                cell.teamImage.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.lightGray
        }
        cell.teamLabel.text = standings[indexPath.row].name
        cell.GPLabel.text = "\(standings[indexPath.row].played!)"
        cell.WLabel.text = "\(standings[indexPath.row].win!)"
        cell.TLabel.text = "\(standings[indexPath.row].loss!)"
        cell.LLabel.text = "\(standings[indexPath.row].draw!)"
        cell.GFLabel.text = "\(standings[indexPath.row].goalsfor!)"
        cell.GALabel.text =  standings[indexPath.row].goalsagainst
        cell.GDLabel.text = standings[indexPath.row].goalsdifference
        cell.PTSLabel.text = "\(standings[indexPath.row].total!)"
        
        return cell
    }
    
    // count total standing
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return standings.count
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    // setting height for each row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
   
    func tableView( _ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var noDataImg: UIImageView!
    lazy var standingFresher : UIRefreshControl = {
        let fresherController = UIRefreshControl()
        fresherController.addTarget(self, action: #selector(refreshRequest), for: .valueChanged)
        return fresherController
    }()

    @IBOutlet var table: UITableView!
 
  
     override func viewDidLoad() {
        super.viewDidLoad()
        self.table.backgroundColor = UIColor.clear
        noDataView.isHidden = true
        table.refreshControl = standingFresher
        let getData2 =  tabBarController as! TournamentTabBarViewController
        getTourId = getData2.getTournamentId
        if(getTourId == 1){
                    UserDefaults.standard.setStandingStartDate(start_standing_date: "2018")
                    UserDefaults.standard.setStandingEndDate(end_standing_date: "2019")
        }
        if(getTourId == 2){
            UserDefaults.standard.setStandingStartDate(start_standing_date: "2006")
            UserDefaults.standard.setStandingEndDate(end_standing_date: "2007")
        }
        UserDefaults.standard.setStandingTeam(set_standing_team: "")
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
       
        
    }
    @objc func loadList(notification: NSNotification){
        SVProgressHUD.show(withStatus: "Please Wait")
        self.standings.removeAll()
        self.page = 0
        DispatchQueue.main.async {
//            self.url = "http://127.0.0.1:8000/api/fetchMatches"
            self.check()
        }
    }
    @objc func refreshRequest(){
        check()
        standingFresher.endRefreshing()
    }
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.topItem?.title = "Standing"
        SVProgressHUD.show(withStatus: "Please Wait")
        
        //timer to refresh/feth updated data from api
//        self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(StandingViewController.check), userInfo: nil, repeats: true)
        
        DispatchQueue.main.async{
            self.check()        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.setStandingStartDate(start_standing_date: "")
        UserDefaults.standard.setStandingEndDate(end_standing_date: "")
        UserDefaults.standard.setStandingTeam(set_standing_team: "")
    }
    @objc func check(){
       
        self.fetchStanding(url: "http://127.0.0.1:8000/api/filterStandingBySeason")
        
    }
    // function to fetch standing from api
    @objc func fetchStanding(url : String){
        var standingArray : Dictionary = [String:Any]()
        let getData =  tabBarController as! TournamentTabBarViewController
        getTournamentId = getData.getTournamentId
        standingArray["tournament_id"] = "\(getTournamentId)"
        standingArray["start"] = UserDefaults.standard.getStandingStartDate()
        standingArray["end"] = UserDefaults.standard.getStandingEndDate()
        standingArray["team"] = UserDefaults.standard.getStandingTeam()
//        standingArray["end"] = 2018
        
        Alamofire.request(url, method: .post, parameters: standingArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Standing]>) in
            if (response.result.value?.isEmpty)! == false{
                if((response.result.value)?.count)! != self.standings.count{
                    self.standings.append(contentsOf: response.result.value!)
                    print( self.standings.count)
//                    print(response.result.value![0].season_range)
                    DispatchQueue.main.async {
                        self.table.reloadData()
                        self.StandingTitle.text = "\(UserDefaults.standard.getStandingStartDate())-\(UserDefaults.standard.getStandingEndDate())"
                        SVProgressHUD.dismiss()
                    }
                }
            }
            if self.standings.count == 0{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = false
                }
            }
            else{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = true
                }
            }
            SVProgressHUD.dismiss()
        }
}
}
