
//
//  TeamTableViewController.swift
//  TeamTable
//
//  Created by Tahir on 6/11/18.
//  Copyright © 2018 Tahir. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

var myindex = Int()

class TeamTableViewController: UITableViewController {
    // pull for more method (fresher)
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataImg: UIImageView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    lazy var teamFresher : UIRefreshControl = {
        let fresherController = UIRefreshControl()
        fresherController.addTarget(self, action: #selector(refreshRequest), for: .valueChanged)
        return fresherController
    }()
    var getTournamentId = Int()
    var getTourName = String()
    var isInprogress : Bool = true
    var teams: [Team] = []
    var timer = Timer()
    var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noDataView.isHidden = true
        self.tableView.backgroundColor = UIColor.clear
        //calling function to pull more
        tableView.refreshControl = teamFresher
        
    }
    //function to call pull more request
    
    @objc func refreshRequest(){
        check()
        teamFresher.endRefreshing()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Team"
        //loader when view will appears
        if(getTournamentId > 0){
            print("tournament_id: \(getTournamentId)")
            print("tournament_name: \(getTourName)")
            SVProgressHUD.show(withStatus: "Please Wait")
            //timer to refresh/feth updated data from api
            self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(TeamTableViewController.check), userInfo: nil, repeats: true)
            
            DispatchQueue.main.async{
                self.check()
            }
        }
    }
    // function to check if user is login or not
    @objc func check(){
        let user_id = UserDefaults.standard.getLogedInId()
        if(user_id > 0){
            let Url = "http://127.0.0.1:8000/api/teams/"+"\(user_id)"
            print(Url)
            self.parseTeamApi(Url : Url)
        }
        else {
            let secUrl = "http://127.0.0.1:8000/api/teams"
            print(secUrl)
            self.parseTeamApi(Url : secUrl)
        }
    }
    // function to parse team api
    @objc func parseTeamApi(Url : String)
    {
        var teamArray : Dictionary = [String:Int]()
        let getData =  tabBarController as! TournamentTabBarViewController
        getTournamentId = getData.getTournamentId
        teamArray["tournament_id"] = getTournamentId
        //        print(getTournamentId)
        Alamofire.request(Url, method: .post, parameters: teamArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Team]>) in
            
            if (response.result.value?.isEmpty)! == false{
                if((response.result.value)?.count)! != self.teams.count{
                    self.teams.append(contentsOf: response.result.value!)
                    print( self.teams.count)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
                
            }
            //            if ((response.result.value?.isEmpty)! && self.page >= 0){
            //                self.isInprogress = false
            //            }
            if self.teams.count == 0{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = false
                }
                
            }
            else{
                DispatchQueue.main.async {
                    self.noDataView.isHidden = true
                }
                
            }
            SVProgressHUD.dismiss()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        //disable timer
        timer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    //row height
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    //total count of teams
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
      
        
            return teams.count
        
    }
    // function to show teams data in cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamTableViewCell

            cell.backgroundColor = UIColor.clear
            let imageUrlString = "http://127.0.0.1:8000"+teams[indexPath.row].image!
            //print(imageUrlString)
            if let url = URL(string : imageUrlString){
                do {
                    let data = try Data(contentsOf: url)
                    cell.teamImage.image = UIImage(data: data)
                    
                }catch let err {
                    print(" Error : \(err.localizedDescription)")
                }
            }
            
            cell.layer.cornerRadius = cell.frame.height / 2;
            cell.teamName.text = teams[indexPath.row].name
            cell.countryname.text = teams[indexPath.row].country
            cell.teamId.text =  "\(String(describing: teams[indexPath.row].teamId))"
            if(teams[indexPath.row].favorite == 1){
                cell.teamButton.setImage(UIImage(named: "filled red heart"), for: .normal)
            }
            else{
                cell.teamButton.setImage(UIImage(named: "red heart"), for: .normal)
            }
            cell.teamButton.tag =  teams[indexPath.row].teamId!
            cell.teamButton.addTarget(self, action: #selector(Button_favorite(sender:)), for: .touchUpInside)
        
        return cell
    }
    // button to handle add/remove functionality of favorite in teams
    @objc func Button_favorite(sender: UIButton){
        print(sender.tag)
        //        sender.setImage(UIImage(named: "filled red heart.png"), for: .normal)
        //        sender.isUserInteractionEnabled = true
        
        //        if let btnImage = sender.image(for: .normal),
        //            let Image = UIImage(named: "red heart.png"), UIImagePNGRepresentation(btnImage) == UIImagePNGRepresentation(Image)
        //        {
        //            sender.setImage(UIImage(named:"filled red heart.png"), for: .normal)
        //
        //        }
        //        else
        //        {
        //            sender.setImage( UIImage(named:"red heart.png"), for: .normal)
        //        }
        //
        let team_id = sender.tag
        let user_id = UserDefaults.standard.getLogedInId()
        let type = "team"
        if(user_id > 0){
            SVProgressHUD.show(withStatus: "Please Wait")
            favorite(team_id: team_id , user_id: user_id, type: type)
        }
        else{
            self.alert(title: "Alert", message: "Please Login or register")
        }
    }
    // alert function to show message
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    // sending favorite button response to db
    public func favorite(team_id: Int , user_id : Int , type: String){
        
        var favArray : Dictionary = [String:String]()
        favArray["id"] = "\(team_id)"
        favArray["user_id"] = "\(user_id)"
        favArray["type"] = type
        guard let url = URL(string: "http://127.0.0.1:8000/api/favorite") else { return }
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        guard let httpbody = try? JSONSerialization.data(withJSONObject: favArray, options: []) else { return }
        request.httpBody = httpbody;
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let response = response{
                print(response)
            }
            if let data = data{
                do{
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    print(parsedData)
                    let key = "result"
                    let stateCode = parsedData[key] as? Int ?? Int(parsedData[key] as? String ?? "")
                    print(stateCode!)
                    let result = stateCode!
                    if(result == 0){
                        DispatchQueue.main.async {
                            self.teams.removeAll();
                            self.check()
                        }
                    }
                    if(result == 1){
                        DispatchQueue.main.async {
                            self.teams.removeAll();
                            self.check()
                        }
                    }
                }catch{
                    print(error)
                }
            }
            }.resume()
    }
    // function for pagination in teams
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == teams.count - 1 {
            // we are at last cell load more content
            if isInprogress {
                print("isInprogress")
                self.perform(#selector(self.check), with: nil, afterDelay: 2)
                self.page = self.page + 1
            }
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TeamTabBarController") as! TeamTabBarViewController
        tabBarController.getTeamId = teams[indexPath.row].teamId!
        tabBarController.getImgUrl = teams[indexPath.row].image!
        tabBarController.getTeamName = teams[indexPath.row].name!
        tabBarController.getTeamCountry = teams[indexPath.row].country!
        if let viewControllers = tabBarController.viewControllers,
            let TeamDC = viewControllers.first as? TeamDetailViewController {
            TeamDC.getId = teams[indexPath.row].teamId!
            TeamDC.getImageUrl = teams[indexPath.row].image!
            
        }
        navigationController?.pushViewController(tabBarController, animated: true)
    }
    
}



