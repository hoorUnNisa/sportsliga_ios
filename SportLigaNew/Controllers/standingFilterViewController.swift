//
//  standingFilterViewController.swift
//  SportLigaNew
//
//  Created by Hoor on 17/10/2018.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
@available(iOS 11.0, *)
class standingFilterViewController: UIViewController, UITextFieldDelegate {
    var teams: [Team] = []
    var getTournamentId = Int()
    var startStandingDate = String()
    var endStandingDate = String()
    var teamStanding = String()
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var teamListTxtField: UITextField!
    @IBOutlet weak var endDateTxtField: UITextField!
    @IBOutlet weak var startDateTxtField: UITextField!
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func applyBtn(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
        print(self.teamStanding.isEmpty)
        if((self.startStandingDate < self.endStandingDate) || (self.startStandingDate.isEmpty == true && self.endStandingDate.isEmpty == false) || (self.startStandingDate.isEmpty == false && self.endStandingDate.isEmpty == true) || (self.startStandingDate.isEmpty == true && self.endStandingDate.isEmpty == true && self.teamStanding.isEmpty == false)){
            if(self.startStandingDate.isEmpty == false){
                UserDefaults.standard.setStandingStartDate(start_standing_date: self.startStandingDate)
            }
            else{
                UserDefaults.standard.setStandingStartDate(start_standing_date: "")
            }
            if(self.endStandingDate.isEmpty == false){
                UserDefaults.standard.setStandingEndDate(end_standing_date: self.endStandingDate)
            }
            else{
                UserDefaults.standard.setStandingEndDate(end_standing_date: "")
            }
            if(self.teamStanding.isEmpty == false){
                UserDefaults.standard.setStandingTeam(set_standing_team: self.teamStanding)
            }
            else{
                UserDefaults.standard.setStandingTeam(set_standing_team: "")
            }
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
            
        }else{
            let alert = UIAlertController(title: "Filter", message: "Enter Valid Data", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    @IBAction func clearBtn(_ sender: Any) {

        self.startDateTxtField.text = ""
        self.endDateTxtField.text = ""
        self.teamListTxtField.text = ""
        UserDefaults.standard.setStandingStartDate(start_standing_date: "")
        UserDefaults.standard.setStandingEndDate(end_standing_date: "")
        UserDefaults.standard.setStandingTeam(set_standing_team: "")
        self.startStandingDate = ""
        self.endStandingDate = ""
        self.teamStanding = ""
        print(self.teamStanding.isEmpty)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        teamListTxtField.delegate = self
        clearBtn.layer.borderWidth = 1.0
        clearBtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        self.parseTeamApi(Url: "http://127.0.0.1:8000/api/teams")
        createTeamPicker()
    }
    func createTeamPicker(){
        let tourTeamPicker = UIPickerView()
        tourTeamPicker.delegate = self
        teamListTxtField.inputView = tourTeamPicker
    }
    override func viewWillAppear(_ animated: Bool) {
        self.startDateTxtField.text = UserDefaults.standard.getStandingStartDate()
        self.endDateTxtField.text = UserDefaults.standard.getStandingEndDate()
        self.teamListTxtField.text = UserDefaults.standard.getStandingTeam()
        self.startStandingDate = UserDefaults.standard.getStandingStartDate()
        self.endStandingDate = UserDefaults.standard.getStandingEndDate()
        self.teamStanding = UserDefaults.standard.getStandingTeam()
    }
    func textFieldDidBeginEditing(_ teamTextField: UITextField) {
//        self.teamListTxtField.isUserInteractionEnabled = false
    }
    @objc func parseTeamApi(Url : String)
    {
        var teamArray : Dictionary = [String:Int]()
        teamArray["tournament_id"] = 1
        Alamofire.request(Url, method: .post, parameters: teamArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Team]>) in
            
            if (response.result.value?.isEmpty)! == false{
                if((response.result.value)?.count)! != self.teams.count{
                    self.teams.append(contentsOf: response.result.value!)
                    print( self.teams.count)
                    self.teams = response.result.value!
                }
            }
        }
        
    }
    
    func getTimeIntervalForDate()->(min : Date, max : Date){
        
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.year], from: Date())
        minDateComponent.year = 2001 // Start time
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let minDate = calendar.date(from: minDateComponent)
        print(" min date : \(formatter.string(from: minDate!))")
        
        var maxDateComponent = calendar.dateComponents([.year], from: Date())
        maxDateComponent.year = 2019 //EndTime
        
        
        
        let maxDate = calendar.date(from: maxDateComponent)
        print(" max date : \(formatter.string(from: maxDate!))")
        
        
        
        return (minDate!,maxDate!)
    }
    
    @IBAction func startDate(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        let dates = getTimeIntervalForDate()
        datePickerView.minimumDate = dates.min
        datePickerView.maximumDate = dates.max
        datePickerView.addTarget(self, action: #selector(startDateFormat(sender:)), for: .valueChanged)
    }
    
    @objc func startDateFormat(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        startDateTxtField.text = dateFormatter.string(from: sender.date)
        self.startStandingDate = dateFormatter.string(from: sender.date)
    }
    @IBAction func endDate(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        let dates = getTimeIntervalForDate()
        datePickerView.minimumDate = dates.min
        datePickerView.maximumDate = dates.max
        datePickerView.addTarget(self, action: #selector(endDateFormat(sender:)), for: .valueChanged)
    }
    
    @objc func endDateFormat(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        endDateTxtField.text = dateFormatter.string(from: sender.date)
        self.endStandingDate = dateFormatter.string(from: sender.date)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
@available(iOS 11.0, *)
extension standingFilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return teams.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        teamListTxtField.text = teams[row].name
        self.teamStanding = teams[row].name!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return teams[row].name
    }
}

