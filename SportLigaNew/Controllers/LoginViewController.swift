//
//  ViewController.swift
//  Loginform
//
//  Created by Zahid on 7/19/18.
//  Copyright © 2018 Zahid. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Alamofire
import ObjectMapper
import GoogleSignIn
import SVProgressHUD
class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var google: GIDSignInButton!
    @IBOutlet weak var facebook: UIButton!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
   /* Home to Login page notification method */
    
    @IBAction func loginToHome(_ sender: Any) {
        performSegue(withIdentifier: "LoginToHome", sender: nil)
    }
    
    /* Login method for login functionality  */
    
    @IBAction func login(_ sender: Any) {
        
        self.message.isHidden = true
        var validData = true
       
        guard let Email = email.text, email.text?.count != 0
        else {
            self.message.isHidden = false
            self.message.text = "Plese enter Email"
            validData = false
            return
        }
        if isValidEmail(email: Email) == false {
            self.message.isHidden = false
            self.message.text = "Plese enter valid Email "
            validData = false
        }
       
        guard var _ = password.text, password.text?.count != 0
        else {
            self.message.isHidden = false
            self.message.text = "Plese enter password"
            validData = false
            return
        }
        if validData == true{
            print("data is valid")
//            ProgressHUD.show("Please Wait", interaction: false)
 
            /* After validation send data to server and parse it */
            
            ProgressHUD.show("Please Wait", interaction: false)
            var loginArray : Dictionary = [String:String]()
            loginArray["password"] = self.password.text
            loginArray["email"] = self.email.text
            guard let url = URL(string: "http://127.0.0.1:8000/api/login") else { return }
            var request = URLRequest(url:url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            guard let httpbody = try? JSONSerialization.data(withJSONObject: loginArray, options: []) else { return }
            request.httpBody = httpbody;
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let response = response{
                    print(response)
                }
                if let data = data{
//                    self.loginUser = try! JSONDecoder().decode([LoginUser].self, from: data)
//                    print(self.loginUser[0].first_name)
                    do{
                        let user : NSDictionary = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        if let error = user["error"]{
                            print(error)
                            DispatchQueue.main.async {
                                self.message.isHidden = false
                                self.message.text = "Email or passwod are incorrect"
                            }
                            ProgressHUD.dismiss()
                        } else if let verify_status = user["verify_status"]{
                            //print(user)
                            if verify_status as! Int == 1{
                                UserDefaults.standard.setIsLogedIn(value: true)
                                UserDefaults.standard.setLogedInId(id: user["id"] as! Int)
                                UserDefaults.standard.setLogedInFirstName(first_name: user["first_name"] as! String)
                                UserDefaults.standard.setLogedInLastName(last_name: user["last_name"] as! String)
                                UserDefaults.standard.setLogedInEmail(email: user["email"] as! String)
                                UserDefaults.standard.setLogedInImage(image: user["image"] as! String)
                                UserDefaults.standard.synchronize()
                                //print(UserDefaults.standard.setLogedInImage())
                                ProgressHUD.dismiss()
                                DispatchQueue.main.async {
                                    self.performSegue(withIdentifier: "LoginToHome", sender: nil)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.message.isHidden = false
                                    self.message.text = "Please verify your email"
                                }
                                 ProgressHUD.dismiss()
                            }
                            
                        }
                        
                    }catch{
                        //print(error)
                        DispatchQueue.main.async {
                            self.message.isHidden = false
                            self.message.text = "there is some issue in server please try later"
                        }
                        ProgressHUD.dismiss()
                        //print("there is some issue in server please try later")
                    }
                }
                if let error = error {
                    print(error)
                }
            }.resume()
            
            
            
        }
        
    }
   
    /* Check email is valid */
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        GIDSignIn.sharedInstance().delegate = self
       let userLogedIn = UserDefaults.standard.isLogedIn()
        print(userLogedIn)
        if userLogedIn == true{
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "LoginToHome", sender: nil)
            }
            
        }
        GIDSignIn.sharedInstance().uiDelegate = self
//        let googleBtn = GIDSignInButton()
//        googleBtn.center = view.center
//        view.addSubview(googleBtn)
        self.message?.isHidden = true
        
        email?.self.borderStyle = .none
        email?.self.layer.backgroundColor = UIColor.white.cgColor
        
        email?.self.layer.masksToBounds = false
        email?.self.layer.shadowColor = UIColor.gray.cgColor
        email?.self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        email?.layer.shadowOpacity = 1.0
        email?.self.layer.shadowRadius = 0.0
        //
        //password field
        password?.self.borderStyle = .none
        password?.self.layer.backgroundColor = UIColor.white.cgColor
        
        password?.self.layer.masksToBounds = false
        password?.self.layer.shadowColor = UIColor.gray.cgColor
        password?.self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        password?.layer.shadowOpacity = 1.0
        password?.self.layer.shadowRadius = 0.0
        //
       
        
        self.login?.layer.cornerRadius = 20;
       
        //facebook
        self.facebook?.layer.borderColor = UIColor.blue.cgColor
        self.facebook?.layer.borderColor = UIColor(red: 50/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        
        //login background color
//        self.login?.layer.backgroundColor = UIColor(red: 1/255.0, green: 208/255.0, blue: 153/255.0, alpha: 1.0).cgColor

        self.facebook?.layer.borderWidth = 2
        self.facebook?.layer.cornerRadius = 20;
        
//google
        
        self.google?.layer.cornerRadius = 20;
        self.google?.layer.borderColor = UIColor.orange.cgColor
        self.google?.layer.borderColor = UIColor(red: 255/255.0, green: 110/255.0, blue: 126/255.0, alpha: 1.0).cgColor
        self.google?.layer.borderWidth = 2
    }
    
    /* Login with facebook button functionality */
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
       let loginManager = FBSDKLoginManager()
       loginManager.logOut()
    }
    
    /* Login with Google button functionality */
    
    @IBAction func loginWithGoogle(_ sender: Any) {
         GIDSignIn.sharedInstance().signIn()
    }
    
    /* After click login with google button parse data of user */
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
       
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
//            ProgressHUD.show()
            SVProgressHUD.show()
            var userArray : Dictionary = [String: String]()
            userArray["provider_user_id"] = user.userID
            userArray["first_name"] = user.profile.givenName
            userArray["last_name"] = user.profile.familyName
            userArray["email"] = user.profile.email
            userArray["provider"] = "google"
            print(userArray)
            GIDSignIn.sharedInstance().signOut()
            let urlString = "http://127.0.0.1:8000/api/socialLogin"
            Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                (response : DataResponse<SocialUser>) in
                let socialUser = response.result.value
                UserDefaults.standard.setIsLogedIn(value: true)
                UserDefaults.standard.setLogedInId(id: socialUser!.user_id!)
                UserDefaults.standard.setLogedInFirstName(first_name: socialUser!.firstName!)
                UserDefaults.standard.setLogedInLastName(last_name: socialUser!.lastName! )
                UserDefaults.standard.setLogedInEmail(email: socialUser!.email! )
                UserDefaults.standard.setLogedInImage(image: socialUser!.image!)
                UserDefaults.standard.synchronize()
//                ProgressHUD.dismiss()
                SVProgressHUD.dismiss()
                print(UserDefaults.standard.getLogedInFirstName())
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "LoginToHome", sender: nil)
                }
            }
        }
        
    }
    
    /* After click login with facebook button parse data of user */
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            SVProgressHUD.show()
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let res = result as! Dictionary<String, String>
                    var userArray : Dictionary = [String: String]()
                    print(res)
                    userArray["last_name"] = res["last_name"]
                    userArray["first_name"] = res["first_name"]
                    userArray["email"] = res["email"]
                    userArray["provider_id"] = res["id"]
                    userArray["provider"] = "facebook"
                    print(userArray)
                    let urlString = "http://127.0.0.1:8000/api/socialLogin"
                    Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseObject{
                        (response : DataResponse<SocialUser>) in
                        let socialUser = response.result.value
                        UserDefaults.standard.setIsLogedIn(value: true)
                        UserDefaults.standard.setLogedInId(id: socialUser!.user_id!)
                        UserDefaults.standard.setLogedInFirstName(first_name: socialUser!.firstName!)
                        UserDefaults.standard.setLogedInLastName(last_name: socialUser!.lastName! )
                        UserDefaults.standard.setLogedInEmail(email: socialUser!.email! )
                        UserDefaults.standard.setLogedInImage(image: socialUser!.image!)

                        SVProgressHUD.dismiss()
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "LoginToHome", sender: nil)
                        }
                    }
                }
            })
            
        }
        
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

