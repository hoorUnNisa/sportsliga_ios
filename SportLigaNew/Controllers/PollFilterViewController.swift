//
//  PollFilterViewController.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/18/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

class PollFilterViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource {
    var pollArray : SearchPoll?
    var pollCount = 0
    var seasonCount = 0
    var season_year = String()
    var season_id = Int()
    @IBOutlet weak var pollSeasonField: UITextField!
    @IBOutlet weak var pollTitleField: UITextField!
    var pollSeasonFieldPiker: UIPickerView!
    var pollTitleFieldPiker: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        pollSeasonFieldPiker = UIPickerView()
        pollTitleFieldPiker = UIPickerView()
        pollSeasonFieldPiker.delegate = self
        pollTitleFieldPiker.delegate = self
        pollSeasonField.inputView = pollSeasonFieldPiker
        pollTitleField.inputView = pollTitleFieldPiker
        pollSeasonFieldPiker.tag = 0
        pollTitleFieldPiker.tag = 1
        pollSeasonField.tag = 0
        pollTitleField.tag = 1
        // Do any additional setup after loading the view.
    }
    @IBAction func searchPoll(_ sender: Any) {
        print("season_id")
        print(season_id)
        UserDefaults.standard.setPollTitle(poll_title: self.pollTitleField.text!)
        if(season_id == 0){
            UserDefaults.standard.setSeasonId(season_id: "")
        }else{
            UserDefaults.standard.setSeasonId(season_id: "\(self.season_id)")
            UserDefaults.standard.setSeasonYear(season_year: "\(self.season_year)")
        }
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    @IBAction func clearSearch(_ sender: Any) {
        UserDefaults.standard.setSeasonId(season_id: "")
        UserDefaults.standard.setPollTitle(poll_title: "")
        pollSeasonField.text = ""
        pollTitleField.text = ""
    }
    override func viewWillAppear(_ animated: Bool) {
        pollSeasonField.text = UserDefaults.standard.getSeasonYear()
        pollTitleField.text = UserDefaults.standard.getPollTitle()
        fetchPolls()
    }
    
    func fetchPolls(){
        SVProgressHUD.show(withStatus: "Please Wait")
        let urlString = "http://127.0.0.1:8000/api/fetchPollForSearch"
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseObject{
            (response : DataResponse<SearchPoll>) in
            if(response.response?.statusCode == 200){
                self.pollArray = response.result.value!
                self.pollCount = (self.pollArray?.poll.count)!
                self.seasonCount = (self.pollArray?.season.count)!
                DispatchQueue.main.async {
                    self.pollSeasonFieldPiker.reloadAllComponents()
                    self.pollTitleFieldPiker.reloadAllComponents()
                }
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
    @IBAction func popOut(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView.tag == 0){
            return seasonCount
        }else if(pickerView.tag == 1){
            return pollCount
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            pollSeasonField.text = pollArray?.season[row].season
            pollSeasonFieldPiker.isHidden = true
            season_id = (pollArray?.season[row].season_id)!
            season_year = (pollArray?.season[row].season)!
        }else if(pickerView.tag == 1){
            pollTitleField.text = pollArray?.poll[row].poll_title
            pollTitleFieldPiker.isHidden = true
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0 && seasonCount != 0){
            return pollArray?.season[row].season
        }else if(pickerView.tag == 1 && pollCount != 0){
            return pollArray?.poll[row].poll_title
        }
        return ""
    }
    
}



