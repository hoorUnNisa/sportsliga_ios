//
//  TournamentTableViewCell.swift
//  SportsLiga
//
//  Created by Ali Iqbal on 6/11/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TournamentTableViewCell: UITableViewCell {

    @IBOutlet weak var TournamentLocation: UILabel!
    @IBOutlet weak var TournamentName: UILabel!
    @IBOutlet weak var TournamentLogo: UIImageView!
    @IBOutlet weak var tournamentButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
