//
//  FilterViewController.swift
//  SportLigaNew
//
//  Created by Tahir on 10/17/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

@available(iOS 11.0, *)
class FilterViewController: UIViewController,UITextFieldDelegate {
    var teams: [Team] = []
    var getTournamentId = Int()
    var startDate = String()
    var endDate = String()
    var team = String()
    var teamName = String()
//    @IBAction func teamFieldAction(_ sender: Any) {
//        self.teamPicker.isHidden = false
//    }
    
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var teamTextField: UITextField!
    @IBOutlet weak var clearBtn: UIButton!
    @IBAction func clearButton(_ sender: Any) {
        self.startDateTextField.text = ""
        self.endDateTextField.text = ""
        self.teamTextField.text = ""
        UserDefaults.standard.setStartDate(start_date: "")
        UserDefaults.standard.setEndDate(end_date: "")
        UserDefaults.standard.setTeam(set_team: "")
        UserDefaults.standard.setTeamName(set_team_name: "")
        self.startDate = ""
        self.endDate = ""
        self.teamName = ""
        self.team = ""
    }
    @IBAction func popOut(_ sender: Any) {
        print(self.startDate.isEmpty)
        print(self.endDate.isEmpty)
        print(self.startDate < self.endDate)
        
//        if(((self.startDate < self.endDate) ||  (self.startDate.isEmpty == false && self.endDate.isEmpty == true) ||  (self.startDate.isEmpty == true && self.endDate.isEmpty == true)) ||  ((self.startDate.isEmpty == false && self.endDate.isEmpty == false ))){
//
        if(((self.startDate < self.endDate) && (self.startDate.isEmpty == false && self.endDate.isEmpty == false)) ||  (self.startDate.isEmpty == false && self.endDate.isEmpty == true) || (self.startDate.isEmpty == true && self.endDate.isEmpty == true)){
            
            print(self.startDate + " " + self.endDate + " " + self.team)
            if(self.startDate.isEmpty == false){
                UserDefaults.standard.setStartDate(start_date: self.startDate)
            }
            if(self.endDate.isEmpty == false){
                UserDefaults.standard.setEndDate(end_date: self.endDate)
            }
            if(self.team.isEmpty == false){
                UserDefaults.standard.setTeam(set_team: self.team)
            }
            if(self.teamName.isEmpty == false){
                UserDefaults.standard.setTeamName(set_team_name: self.teamName)
            }
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
//            }
//            else{
//                self.alert(title: "Alert", message: "Please Select Valid dates")
//            }
        }
        else{
            self.alert(title: "Alert", message: "Please Select Valid dates")
        }
        
    }
    @IBAction func closeButton(_ sender: Any) {
              dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backButton(_ sender: Any) {
     dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamTextField.delegate = self
        clearBtn.layer.borderWidth = 1.0
        clearBtn.layer.borderColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        self.parseTeamApi(Url: "http://127.0.0.1:8000/api/teams")
        createTeamPicker()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func textFieldDidBeginEditing(_ teamTextField: UITextField) {
//        self.teamTextField.isUserInteractionEnabled = false
//    }

    override func viewWillAppear(_ animated: Bool) {
        self.startDateTextField.text = UserDefaults.standard.getStartDate()
        self.endDateTextField.text = UserDefaults.standard.getEndDate()
        self.teamTextField.text = UserDefaults.standard.getTeamName()
    }
    @objc func parseTeamApi(Url : String)
    {
        var teamArray : Dictionary = [String:Int]()
//        let getData =  tabBarController as! TournamentTabBarViewController?
//        getTournamentId = (getData?.getTournamentId)!
        teamArray["tournament_id"] = 1
        //        print(getTournamentId)
        Alamofire.request(Url, method: .post, parameters: teamArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Team]>) in
            
            if (response.result.value?.isEmpty)! == false{
                if((response.result.value)?.count)! != self.teams.count{
                    self.teams.append(contentsOf: response.result.value!)
                    print( self.teams.count)
                    self.teams = response.result.value!
//                    DispatchQueue.main.async {
////                        self.team = response.result.value?[0]
//                        self.teamPicker.reloadAllComponents()
//                    }
                }
                
            }
        }
        
    }
    func getTimeIntervalForDate()->(min : Date, max : Date){
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.year], from: Date())
        minDateComponent.year = 2017 // Start date
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mma"
        let minDate = calendar.date(from: minDateComponent)
//        print(" min date : \(formatter.string(from: minDate!))")
        var maxDateComponent = calendar.dateComponents([.year], from: Date())
        maxDateComponent.year = 2020 //End date
        let maxDate = calendar.date(from: maxDateComponent)
//        print(" max date : \(formatter.string(from: maxDate!))")
        return (minDate!,maxDate!)
    }
    
    @IBAction func startDate(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        let dates = getTimeIntervalForDate()
        datePickerView.minimumDate = dates.min
        datePickerView.maximumDate = dates.max
        datePickerView.addTarget(self, action: #selector(startDateFormat(sender:)), for: .valueChanged)
    }
    
    @objc func startDateFormat(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        startDateTextField.text = dateFormatter.string(from: sender.date)
        self.startDate = dateFormatter.string(from: sender.date)
    }
    @IBAction func endDate(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        let dates = getTimeIntervalForDate()
        datePickerView.minimumDate = dates.min
        datePickerView.maximumDate = dates.max
        datePickerView.addTarget(self, action: #selector(endDateFormat(sender:)), for: .valueChanged)
    }
    
    @objc func endDateFormat(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        endDateTextField.text = dateFormatter.string(from: sender.date)
        self.endDate = dateFormatter.string(from: sender.date)
        
    }
    
    //team picker
    func createTeamPicker(){
        let tourTeamPicker = UIPickerView()
        tourTeamPicker.delegate = self
        teamTextField.inputView = tourTeamPicker
    }
    
    // alert function to show message
    public func alert(title:String , message: String){
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    // returns the number of 'columns' to display.
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
@available(iOS 11.0, *)
extension FilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return teams.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        teamTextField.text = teams[row].name
        self.team = "\(String(describing: teams[row].teamId!))"
        self.teamName = teams[row].name!
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return teams[row].name
    }
}
