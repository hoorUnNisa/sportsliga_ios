//
//  CommentClickViewController.swift
//  SportLigaNew
//
//  Created by Admin on 02/02/1440 AH.
//  Copyright © 1440 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire
class CommentClickViewController: UIViewController {
    
    var commentId   = Int()
    var commentText = String()

    
    @IBOutlet weak var updateCommentArea: UITextView!
    
    @IBAction func updateComment(_ sender: Any) {
   
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if updateCommentArea.text == ""{
            
            self.alertErrorUpdatingCommentWithNull()
            return
        }else{
            ProgressHUD.show("wait updating...", interaction: false)
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let MatchDetailVC = storyBoard.instantiateViewController(withIdentifier: "MatchDetailViewController") as! MatchDetailViewController
            var commentArray : Dictionary<String, Any> = [:]
            commentArray["commentId"] = self.commentId
            commentArray["updatedComment"] = updateCommentArea.text
            let myUrl = URL(string: "http://127.0.0.1:8000/api/comment/update");
            var request = URLRequest(url:myUrl!)
            request.httpMethod = "POST"// Compose a query string
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            guard let httpbody = try? JSONSerialization.data(withJSONObject: commentArray, options: []) else { return }
            request.httpBody = httpbody
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if error != nil
                {
                    print("error=\(String(describing: error))")
                    return
                }
                // print out response object
                //print("response = \(response)")
                //Let's convert response sent from a server side script to a NSDictionary object:
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = json {
                        if parseJSON["update"] != nil{
                            ProgressHUD.dismiss()
                            self.dismiss(animated: true, completion: nil)
                          // self.present(MatchDetailVC, animated: true, completion: nil)
                           // self.alertCommentUpdated()
                            
                            return
                        }else {
                            if parseJSON["error"] != nil{
                                ProgressHUD.dismiss()
                              //  self.showMatchCommentsApiCall()
                                self.alertErrorUpdatingComment()
                                
                            }
                        }
                    }
                } catch {
                    print(error)
                }
            }
            task.resume()
        }
    }
    //show when user try to update comment with empty value
    public func alertErrorUpdatingCommentWithNull(){
        let alertController = UIAlertController(title: "Error!", message: "Comment can't be updated with empty value Enter some text. Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    //showed when comment updation Error occured
    public func alertErrorUpdatingComment(){
        let alertController = UIAlertController(title: "Error!", message: "Error updating comment. Preess Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    //showed when comment is succesfully updated
    public func alertCommentUpdated(){
        let alertController = UIAlertController(title: "Comment Updated:", message: "Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func closeUpdateView(_ sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
  updateCommentArea.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        updateCommentArea.text = commentText
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
