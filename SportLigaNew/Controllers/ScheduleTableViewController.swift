//
//  ScheduleTableViewController.swift
//  schedule
//
//  Created by Tahir on 6/11/18.
//  Copyright © 2018 Tahir. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
class ScheduleTableViewController: UITableViewController {
    // create UIRefresherControl Variable to use in pull more
    lazy var scheduleFresher : UIRefreshControl = {
        let fresherController = UIRefreshControl()
        fresherController.addTarget(self, action: #selector(refreshRequest), for: .valueChanged)
        return fresherController
    }()
    
    var match: [Schedule] = []
    let url = "http://127.0.0.1:8000/api/fetchMatches"
    
    var isInProgress : Bool = true
    var timer = Timer()
    var page = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // calling pull more function
        self.tableView.backgroundColor = UIColor.clear
       tableView.refreshControl = scheduleFresher
    }

    // function to pull more and end fetching
    @objc func refreshRequest(){
        fetchSchedule()
        scheduleFresher.endRefreshing()
    }
    //calling function to fetch data from api
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.topItem?.title = "Schedule"
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
//        self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ScheduleTableViewController.fetchSchedule), userInfo: nil, repeats: true)
        DispatchQueue.main.async {
            self.fetchSchedule()
        }
    }

    //function to fetch schedule data from api
    @objc func fetchSchedule(){
        let Url = "http://127.0.0.1:8000/api/fetchMatches"
        var matchArray : Dictionary = [String:Int]()
        matchArray["tournament_id"] = 1
        matchArray["page"] = page
        Alamofire.request(Url, method: .post, parameters: matchArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Schedule]>) in
            if(response.result.value?.isEmpty)! == false{
//                if((response.result.value)?.count)! != self.match.count{
                    self.match.append(contentsOf: response.result.value!)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
//                }
            }
            if ((response.result.value?.isEmpty)! && self.page > 0)
            {
                self.isInProgress = false
            }
//            ProgressHUD.dismiss()
            SVProgressHUD.dismiss()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
//        timer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    //return total count of match
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  match.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell", for: indexPath) as! ScheduleTableViewCell
        //team one image
        let imageUrlString = "http://127.0.0.1:8000"+match[indexPath.row].team_one_image!
        if let url = URL(string : imageUrlString){
            do {
                let data = try Data(contentsOf: url)
                cell.FirstTeamLogo.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        //team two image
        let imageUrlString1 = "http://127.0.0.1:8000"+match[indexPath.row].team_two_image!
        
        if let url = URL(string : imageUrlString1){
            do {
                let data = try Data(contentsOf: url)
                cell.SecondTeamLogo.image = UIImage(data: data)
                
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        cell.FirstTeamLabel.text = match[indexPath.row].first_team_name
        cell.MatchDate.text = match[indexPath.row].date
        cell.SecondTeamLabel.text = match[indexPath.row].second_team_name
        cell.Location.text = match[indexPath.row].start_time
        cell.matchId.text = "\(match[indexPath.row].id)"
        cell.matchId.isHidden = true
        return cell
    }
    //sanding data to match details
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DvC = storyBoard.instantiateViewController(withIdentifier: "MatchDetailViewController") as! MatchDetailViewController
//        let id:Int = Int(self.teams[indexPath.row].id)
        DvC.getMatchId = (match[indexPath.row].id)!
        DvC.getFirstTeamImgUrl = (match[indexPath.row].team_one_image)!
        DvC.getSecondteamImgUrl = match[indexPath.row].team_two_image!
        DvC.getFirstTeamName = match[indexPath.row].first_team_name!
        DvC.getSecondTeamName = match[indexPath.row].second_team_name!
        DvC.getStartTime = match[indexPath.row].start_time!
        DvC.getStartDate = match[indexPath.row].date!
        self.navigationController?.pushViewController(DvC, animated: true)
    }
    
    //pagination in schedule
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == match.count - 1 {
            if isInProgress {
                print("continue fetching")
                self.page = self.page + 1
                self.perform(#selector(self.fetchSchedule), with: nil, afterDelay: 3)
                //load more
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                //spinner at last row of page
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
            }
            
        }
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



