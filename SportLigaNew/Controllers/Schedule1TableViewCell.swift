//
//  Schedule1TableViewCell.swift
//  SportLigaNew
//
//  Created by Tahir on 10/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class Schedule1TableViewCell: UITableViewCell {
    @IBOutlet weak var MatchDate: UILabel!
    
    @IBOutlet weak var matchId: UILabel!
    @IBOutlet weak var FirstTeamLabel: UILabel!
    
    @IBOutlet weak var SecondTeamLogo: UIImageView!
    @IBOutlet weak var SecondTeamLabel: UILabel!
    @IBOutlet weak var FirstTeamLogo: UIImageView!
    @IBOutlet weak var Location: UILabel!
    @IBOutlet weak var TeamOneGoal: UILabel!
    @IBOutlet weak var TeamTwoGoal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
