//
//  PlayerTableViewCell.swift
//  PlayerScreen
//
//  Created by hidayatullah on 18/07/2018.
//  Copyright © 2018 hidayatullah. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var playerButton: UIButton!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    
    @IBOutlet weak var teamName: UILabel!
    //    @IBOutlet weak var addFavourite: UIButton!
//    @IBOutlet weak var country: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
