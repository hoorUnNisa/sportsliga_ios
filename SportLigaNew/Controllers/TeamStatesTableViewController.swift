//
//  TeamStatesTableViewController.swift
//  TeamStausScreen
//
//  Created by hidayatullah on 19/07/2018.
//  Copyright © 2018 hidayatullah. All rights reserved.
//

import UIKit
import SVProgressHUD
class TeamStatesTableViewController: UITableViewController {
    var label = "Stats"
    var TeamID = Int()
    var matchesPlayed = Int()
    var points = Int()
    var win     = Int()
    var Losses = Int()
    var draw = Int()
    var goals = Int()
    var goalsAgainst = Int()
    var goalsDifference = Int()
    var imgUrl     = String()
    var teamName = String()
    var teamCountry = String()
    override func viewDidLoad() {
        super.viewDidLoad()
self.tableView.backgroundColor = UIColor.clear
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    public func fetchStandingApiCall(){//Fetch Team states from Api
    
        let tabBarC = tabBarController as! TeamTabBarViewController
        self.imgUrl = tabBarC.getImgUrl
        self.teamName = tabBarC.getTeamName
        self.teamCountry = tabBarC.getTeamCountry
//        ProgressHUD.show("Please Wait", interaction: false)
        SVProgressHUD.show(withStatus: "Please Wait")
        guard let url = URL(string: "http://127.0.0.1:8000/api/teamStanding/\(tabBarC.getTeamId)") else {return}
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
               // print(jsonResponse) //Response result
                guard let res = jsonResponse as? [String: Any] else {return }
                let played = res["played"] as? Int
                let wins   = res["win"] as? Int
                let loss   = res["loss"] as? Int
                let draw   = res["draw"] as? Int
                let goal   = res["goalsfor"] as? Int
//                _  = (res["goalsagainst"] as! NSString).integerValue
//                _ = (res["goalsdifference"] as! NSString).integerValue
                let total     = res["total"] as? Int
                if played != nil {
                self.matchesPlayed   = res["played"] as! Int
                }
                if wins != nil{
                    self.win             = res["win"]  as! Int
                }
                if loss != nil{
                    self.Losses          = res["loss"] as! Int
                }
                if draw != nil {
                    self.draw            = res["draw"] as! Int
                }
                if goal != nil {
                    self.goals           = res["goalsfor"] as! Int
                }
                if res["goalsagainst"] != nil{
                self.goalsAgainst    = ((res["goalsagainst"] as? NSString)?.integerValue)!
                }
                if res["goalsdifference"] != nil{
                    self.goalsDifference = ((res["goalsdifference"] as? NSString)?.integerValue)!
                }
                if total != nil {
                    self.points          = res["total"] as! Int
                }
                
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
            
        }
        task.resume()
    
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchStandingApiCall()
//        ProgressHUD.dismiss()
        SVProgressHUD.dismiss()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
        
    }
    func tableView(tableView: UITableView,
                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamStats", for: indexPath) as!
        TeamStatsTableViewCell
        cell.layer.borderWidth = 0.5
      
        if indexPath.row == 0 {
            
             
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 1)
            cell.teamImg.layer.borderWidth = 1
            cell.teamImg.layer.masksToBounds = false
            cell.teamImg.layer.borderColor = UIColor.black.cgColor
            cell.teamImg.layer.cornerRadius = cell.teamImg.frame.height/2
            cell.teamImg.center = view.center
            cell.teamImg.clipsToBounds = true
            cell.firstLabel.text = self.teamName
            cell.secondLabel.text = self.teamCountry
            cell.secondLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.thirdLabel.isHidden = true
            cell.fourthLabel.isHidden = true
            cell.firstLabel.backgroundColor = .clear
            cell.firstLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            let imageUrlString = "http://127.0.0.1:8000" + imgUrl
            if let url = URL(string : imageUrlString){
                do {
                    let data = try Data(contentsOf: url)
                    cell.teamImg.image = UIImage(data: data)

                }catch let err {
                    print(" Error : \(err.localizedDescription)")
                }
            }
        }
        else if indexPath.row == 1{
            
            cell.firstLabel.text  = "Matches Played"
            cell.secondLabel.text = "Points"
           cell.stats.isHidden = true
           cell.thirdLabel.text  = "\(self.matchesPlayed)"
           cell.fourthLabel.text = "\(self.points)"
        }
        else if indexPath.row == 2{
            cell.stats.isHidden = true
            cell.firstLabel.text  = "Winnings"
            cell.secondLabel.text = "Losses"
            cell.thirdLabel.text  = "\(self.win)"
            cell.fourthLabel.text = "\(self.Losses)"
        }
        else if indexPath.row == 3{
            cell.stats.isHidden = true
            cell.firstLabel.text  = "Draw"
            cell.secondLabel.text = "Goals"
            cell.thirdLabel.text  = "\(self.draw)"
            cell.fourthLabel.text = "\(self.goals)"
        }
        else if indexPath.row == 4{
            cell.stats.isHidden = true
            cell.firstLabel.text  = "Goals Against"
            cell.secondLabel.text = "Goals Difference"
            cell.thirdLabel.text  = "\(self.goalsAgainst)"
            cell.fourthLabel.text = "\(self.goalsDifference)"
        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
