//
//  ScheduleTableViewCell.swift
//  schedule
//
//  Created by Tahir on 6/11/18.
//  Copyright © 2018 Tahir. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var MatchDate: UILabel!
    
    @IBOutlet weak var matchId: UILabel!
    @IBOutlet weak var FirstTeamLabel: UILabel!
    
    @IBOutlet weak var SecondTeamLogo: UIImageView!
    @IBOutlet weak var SecondTeamLabel: UILabel!
    @IBOutlet weak var FirstTeamLogo: UIImageView!
    @IBOutlet weak var Location: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
