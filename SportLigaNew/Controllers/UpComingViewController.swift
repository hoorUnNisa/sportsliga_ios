//
//  UpComingViewController.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import AlamofireObjectMapper

class UpComingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataImg: UIImageView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    @IBOutlet weak var matchTable: UITableView!
    var matches : [Match] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noDataView.isHidden   = true
        matchTable.delegate   = self
        matchTable.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Match"
        fetchDate()
    }
    
    /* fetch up comming matches and parse  */
    
    func fetchDate(){
        SVProgressHUD.show(withStatus: "Please Wait")
        matches = []
        let urlString = "http://127.0.0.1:8000/api/upCommingMatches"
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Match]>) in
            if(response.response?.statusCode == 200){
                self.matches.append(contentsOf: response.result.value!)
                DispatchQueue.main.async {
                    self.matchTable.reloadData()
                     SVProgressHUD.dismiss()
                }
            }
            if self.matches.count == 0{
            self.noDataView.isHidden = false
            }else{
                self.noDataView.isHidden = true
            }
//            SVProgressHUD.dismiss()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matches.count
    }
    
    /* show matches in cells */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "UpComingTableViewCell", for: indexPath) as! UpComingTableViewCell
        cell.awayTeamName.text = matches[indexPath.row].awayTeamName
        cell.homeTeamName.text = matches[indexPath.row].homeTeamName
        if let url = URL(string : "http://127.0.0.1:8000"+matches[indexPath.row].awayTeamImage!){
            do {
                let data = try Data(contentsOf: url)
                cell.awayTeamImage.image = UIImage(data: data)
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        if let url = URL(string : "http://127.0.0.1:8000"+matches[indexPath.row].homeTeamImage!){
            do {
                let data = try Data(contentsOf: url)
                cell.homeTeamImage.image = UIImage(data: data)
            }catch let err {
                print(" Error : \(err.localizedDescription)")
            }
        }
        cell.matchDate.text = matches[indexPath.row].matchDate
        cell.matchTime.text = matches[indexPath.row].matchTime
        return cell
    }
    
    /* add data to each table cell */
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(matches[indexPath.row].status == "live"){
            let liveController = storyboard.instantiateViewController(withIdentifier: "LiveScoreViewController") as! LiveScoreViewController
            liveController.homeTeamId = self.matches[indexPath.row].homeTeamId
            liveController.awayTeamId = self.matches[indexPath.row].awayTeamId
            self.navigationController?.pushViewController(liveController, animated: true)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
