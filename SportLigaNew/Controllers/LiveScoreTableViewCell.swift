//
//  LiveScoreTableViewCell.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/5/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class LiveScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var secondTeamPlayerName: UILabel!
    @IBOutlet weak var secondTeamScoreTime: UILabel!
    @IBOutlet weak var scoreType: UIImageView!
    @IBOutlet weak var firstTeamScoreTime: UILabel!
    @IBOutlet weak var firstTeamPlayerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
