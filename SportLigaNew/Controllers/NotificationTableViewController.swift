//
//  NotificationTableViewController.swift
//  NotificationTable
//
//  Created by Zahid on 7/18/18.
//  Copyright © 2018 Zahid. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
class NotificationTableViewController: UITableViewController {
    @IBOutlet var notificationTableView: UITableView!
    lazy var refresher : UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshRequest), for: .valueChanged)
        return refreshControl
    }()
    var notification : [Notification] = []
    var pages: Int = 0
    var scroll: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.refreshControl = refresher
    }
    @objc func refreshRequest(){
        apiRequest()
        refresher.endRefreshing()
    }
    
    /* fecth notification and parse it */
    
    @objc func apiRequest(){
//        ProgressHUD.show()
        SVProgressHUD.show()
        var userArray : Dictionary = [String:Int]()
        userArray["user_id"] = UserDefaults.standard.getLogedInId()
        userArray["page"] = pages
        let urlString = "http://127.0.0.1:8000/api/notification"
        
        Alamofire.request(urlString, method: .post, parameters: userArray, encoding: JSONEncoding.default, headers: nil).responseArray{
            (response : DataResponse<[Notification]>) in
            if (response.result.value?.isEmpty)! == false{
                self.notification.append(contentsOf: response.result.value!)
                
                print( self.notification.count)
                DispatchQueue.main.async {
                    self.notificationTableView.reloadData()
                    //ProgressHUD.dismiss()
                }
            }
            if ((response.result.value?.isEmpty)! && self.pages >= 0){
                self.scroll = false
                print("scroll false ho raha ha")
            }
//            ProgressHUD.dismiss()
            SVProgressHUD.dismiss()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Notifications"
        apiRequest()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notification.count
    }
    
    /* Show notification cell and single notification */
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.firstTeamName.text = self.notification[indexPath.row].firstTeamName
        cell.secondTeamName.text = self.notification[indexPath.row].secondTeamName
        cell.result.text = self.notification[indexPath.row].result
        cell.createdAt.text = self.notification[indexPath.row].createAt
        return cell
        
    }
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(scrollView.contentOffset.y)
//        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
//            //print("pages \(pages)")
//            self.perform(#selector(self.apiRequest), with: nil, afterDelay: 2)
//        }
//    }
    
    /* Pagination for notifications */
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == notification.count - 1 {
            // we are at last cell load more content
            if scroll {
                print(scroll)
                self.perform(#selector(self.apiRequest), with: nil, afterDelay: 2)
                self.pages = self.pages + 1
                
                // for load more indicatore
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.notificationTableView.tableFooterView = spinner
                self.notificationTableView.tableFooterView?.isHidden = false
            }
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

