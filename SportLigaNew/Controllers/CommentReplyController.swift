//
//  CommentReplyController.swift
//  SportLigaNew
//
//  Created by Zahid on 10/15/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit
import Alamofire

class CommentReplyController: UIViewController {
   
    var comment_id    = Int()
    var replytext = String()
    var against_user = String()
    var user_id = Int()
    var match_id = Int()
    
    @IBOutlet weak var textArea: UITextView!
    @IBOutlet weak var user_name: UILabel!
    
    @IBAction func backbutton(_ sender: Any) {
             self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submit(_ sender: Any) {
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                if textArea.text == ""{
        
                    self.alertReplyAreaWithNull()
                    return
                }else{
                    ProgressHUD.show("pleas wait")
                    self.user_name.text = self.against_user
                    var replyArray : Dictionary<String, Any> = [:]
                    replyArray["user_id"] = self.user_id
                    replyArray["comment_id"] = self.comment_id
                    replyArray["match_id"]   = self.match_id
                    print(self.match_id)
                    replyArray["text"] = textArea.text
                    //replyArray["userName"] = self.against_user
                    let url = "http://127.0.0.1:8000/api/createCommentReply"
                    Alamofire.request(url, method: .post, parameters: replyArray, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                        let status = response.result.value
                    print(status)
                        //                let error = response.result.value
                        //                let errorstatus = error!["erorr"]
                        ProgressHUD.dismiss()
                        self.dismiss(animated: true, completion: nil)
                      //  self.present(MatchDetailVC, animated:true, completion:nil)
                    }
                }
    }
    
    public func alertreplySuccesfull(){
        let alertController = UIAlertController(title: "Comment Updated:", message: "Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    public func alertReplyAreaWithNull(){
        let alertController = UIAlertController(title: "Error!", message: "Comment Area can't be null value Enter some text. Press Ok to continue.", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            
            
        }
        alertController.addAction(confirmAction)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         self.user_name.text = self.against_user
    //
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
