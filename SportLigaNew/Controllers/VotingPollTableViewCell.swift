//
//  VotingPollTableViewCell.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 9/25/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import UIKit

class VotingPollTableViewCell: UITableViewCell {
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var totalVotes: UILabel!
    var playerId : String?
    @IBOutlet weak var voteButton: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var teamName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.playerImage.layer.cornerRadius = self.playerImage.frame.size.width/2
        self.playerImage.clipsToBounds = true
        // Configure the view for the selected state
    }

}
