//
//  Standing.swift
//  SportLigaNew
//
//  Created by Hoor on 18/10/2018.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Standing: Mappable {
    var name: String?
    var played: Int?
    var win: Int?
    var loss: Int?
    var draw: Int?
    var Image: String?
    var goalsfor: Int?
    var goalsagainst: String?
    var goalsdifference: String?
    var total: Int?
    var season_range: Int?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        name <- map["name"]
        played <- map["played"]
        win <- map["win"]
        loss <- map["loss"]
        draw <- map["draw"]
        Image <- map["Image"]
        goalsfor <- map["goalsfor"]
        goalsagainst <- map["goalsagainst"]
        goalsdifference <- map["goalsdifference"]
        total <- map["total"]
        season_range <- map["season_range"]
    }
    
}
