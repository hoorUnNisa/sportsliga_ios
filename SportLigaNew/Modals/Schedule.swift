//
//  Schedule.swift
//  SportLigaNew
//
//  Created by Hoor on 9/25/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Schedule: Mappable {
    var id:              Int?
    var api_id:          Int?
    var team_one_image:  String?
    var team_two_image:  String?
    var first_team_name: String?
    var second_team_name:String?
    var team_one_score:  String?
    var team_two_score:  String?
    var date:            String?
    var start_time:      String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id               <- map["id"]
        api_id           <- map["api_id"]
        team_one_image   <- map["team_one_image"]
        team_two_image   <- map["team_two_image"]
        first_team_name  <- map["first_team_name"]
        second_team_name <- map["second_team_name"]
        team_one_score   <- map["team_one_score"]
        team_two_score   <- map["team_two_score"]
        date             <- map["date"]
        start_time       <- map["start_time"]
        
        
    }
    
    
}
