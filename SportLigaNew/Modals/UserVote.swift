//
//  UserVote.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 9/27/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class UserVote : Mappable{
    var user_id : Int?
    var competitor_id : Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        competitor_id <- map["competitor_id"]
    }
    
    
}

