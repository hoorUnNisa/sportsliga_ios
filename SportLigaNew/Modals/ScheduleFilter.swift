//
//  ScheduleFilter.swift
//  SportLigaNew
//
//  Created by Tahir on 10/18/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation

var str = ""
extension UserDefaults{
    
    //set start date for scheule
    func setStartDate(start_date:String){
        set(start_date, forKey: "start_date")
    }
    
    //set end date for scheule
    func setEndDate(end_date:String){
        set(end_date, forKey: "end_date")
    }
    
    //set tem for scheule
    func setTeam(set_team:String){
        set(set_team, forKey: "set_team")
    }
    
    //set tem for scheule
    func setTeamName(set_team_name:String){
        set(set_team_name, forKey: "set_team_name")
    }
    
    //get start date for scheule
    func getStartDate() -> String {
        if(string(forKey: "start_date")?.isEmpty == false){
            return string(forKey: "start_date")!
        }
        else{
            return str
        }
    }
    
    //get end date for scheule
    func getEndDate() -> String {
        if(string(forKey: "end_date")?.isEmpty == false){
            return string(forKey: "end_date")!
        }
        else{
            return str
        }
    }
    
    //get team for scheule
    func getTeam() -> String {
        if(string(forKey: "set_team")?.isEmpty == false){
            return string(forKey: "set_team")!
        }
        else{
            return str
        }
    }
    
    //get team for scheule
    func getTeamName() -> String {
        if(string(forKey: "set_team_name")?.isEmpty == false){
            return string(forKey: "set_team_name")!
        }
        else{
            return str
        }
    }
}
