//
//  LiveScore.swift
//  SportLigaNew
//
//  Created by Admin on 14/01/1440 AH.
//  Copyright © 1440 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class LiveScore: Mappable{
    
    var firstTeamName : String?
    var secondTeamName : String?
    var firstTeamGoals : String?
    var secondTeamGoals : String?
    var secondTeamImage : String?
    var firstTeamImage : String?
    var time :String?
    var score : [Score]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstTeamName <- map["home_team"]
        secondTeamName <- map["away_team"]
        firstTeamGoals <- map["home_team_goals"]
        secondTeamGoals <- map["away_team_goals"]
        secondTeamImage <- map["away_team_image"]
        firstTeamImage <- map["home_team_image"]
        time <- map["match_time"]
        score <- map["score"]
    }
}
