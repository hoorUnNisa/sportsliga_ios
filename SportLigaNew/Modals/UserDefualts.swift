//
//  UserDefualts.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 7/31/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation

extension UserDefaults{
    func setIsLogedIn(value:Bool){
        set(value, forKey: "isLogedIn")
        synchronize()
    }
    
    func setLogedInId(id:Int){
        set(id, forKey: "user_id")
        synchronize()
    }
    
    func setLogedInFirstName(first_name:String){
        set(first_name, forKey: "user_first_name")
        synchronize()
    }
    func setLogedInLastName(last_name:String){
        set(last_name,forKey: "user_last_name")
        synchronize()
    }
    func setLogedInEmail(email:String){
        set(email, forKey: "user_email")
        synchronize()
    }
    func setLogedInImage(image:String){
        set(image, forKey: "image")
        synchronize()
    }
    
    func isLogedIn() -> Bool{
        return bool(forKey: "isLogedIn")
    }
    
    func getLogedInFirstName() -> String {
        return string(forKey: "user_first_name")!
    }
    
    func getLogedInLastName() -> String {
        return string(forKey: "user_last_name")!
    }
    
    func getLogedInemail() -> String {
        return string(forKey: "user_email")!
    }
    
    func getLogedInId() -> Int {
        return integer(forKey: "user_id")
    }
    
    func getLogedInImage() -> String {
        return string(forKey: "image")!
//        return String("/assets/frontend/images/players/1.jpg")
    }
}
