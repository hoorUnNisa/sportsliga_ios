//
//  Notification.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 8/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Notification: Mappable {
    
    //var match_id : Int?
    var firstTeamName : String?
    var secondTeamName : String?
    var result : String?
   // var tournamentId : Int?
   // var tournamentName : String?
    //var tournamentBadge : String?
    var createAt : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
       // match_id <- map["match_id"]
        firstTeamName <- map["first_team_name"]
        secondTeamName <- map["second_team_name"]
        result <- map["result"]
        //tournamentId <- map["tournament_id"]
        //tournamentName <- map["tournament_name"]
        //tournamentBadge <- map["tournament_badge"]
        createAt <- map ["created_at"]
        
    }
    
    
}

