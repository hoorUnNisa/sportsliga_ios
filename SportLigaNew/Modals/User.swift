
//
//  User.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 8/7/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class SocialUser: Mappable {
    required init?(map: Map) {
    }
    
    var user_id : Int?
    var firstName : String?
    var lastName : String?
    var email : String?
    var image : String?
        
    func mapping(map: Map) {
        user_id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        image <- map["image"]
    }
    
    
}
