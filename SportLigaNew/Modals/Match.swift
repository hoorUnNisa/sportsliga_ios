//
//  Match.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/3/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Match : Mappable{
    
    var awayTeamName : String?
    var awayTeamImage : String?
    var homeTeamImage : String?
    var homeTeamName : String?
    var homeTeamId : String?
    var awayTeamId : String?
    var matchDate : String?
    var matchTime : String?
    var status : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        awayTeamName <- map["away_team"]
        homeTeamName <- map["home_team"]
        homeTeamImage <- map["home_team_image"]
        awayTeamImage <- map["away_team_image"]
        homeTeamId <- map["home_team_id"]
        awayTeamId <- map["away_team_id"]
        matchDate <- map["match_date"]
        matchTime <- map["match_time"]
        status <- map["status"]
        
    }
    
    
}
