//
//  PollDefualt.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/19/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation

extension UserDefaults{
    func setPollTitle(poll_title:String){
        set(poll_title, forKey: "poll_title")
        synchronize()
    }
    
    func setSeasonId(season_id:String){
        set(season_id, forKey: "season_id")
        synchronize()
    }
    
    func setSeasonYear(season_year:String){
        set(season_year, forKey: "season_year")
        synchronize()
    }
    
    func getPollTitle() -> String {
        if(string(forKey: "poll_title")?.isEmpty == false){
            return string(forKey: "poll_title")!
        }
        else{
            return ""
        }
    }
    func getSeasonId() -> String {
        if(string(forKey: "season_id")?.isEmpty == false){
            return string(forKey: "season_id")!
        }
        else{
            return ""
        }
    }
    
    func getSeasonYear() -> String {
        if(string(forKey: "season_year")?.isEmpty == false){
            return string(forKey: "season_year")!
        }
        else{
            return ""
        }
    }
}

