//
//  StandingFilter.swift
//  SportLigaNew
//
//  Created by Hoor on 18/10/2018.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
var strDefault = ""
extension UserDefaults{
    
    //set start date for standing
    func setStandingStartDate(start_standing_date:String){
        set(start_standing_date, forKey: "start_standing_date")
    }
    
    //set end date for standing
    func setStandingEndDate(end_standing_date:String){
        set(end_standing_date, forKey: "end_standing_date")
    }
    
    //set team for staning
    func setStandingTeam(set_standing_team:String){
        set(set_standing_team, forKey: "set_standing_team")
    }
    
    //get start date for standing
    func getStandingStartDate() -> String {
        if(string(forKey: "start_standing_date")?.isEmpty == false){
            return string(forKey: "start_standing_date")!
        }
        else{
            return strDefault
        }
    }
    
    //get end date for standing
    func getStandingEndDate() -> String {
        if(string(forKey: "end_standing_date")?.isEmpty == false){
            return string(forKey: "end_standing_date")!
        }
        else{
            return strDefault
        }
    }
    
    //get team for standing
    func getStandingTeam() -> String {
        if(string(forKey: "set_standing_team")?.isEmpty == false){
            return string(forKey: "set_standing_team")!
        }
        else{
            return strDefault
        }
    }
}
