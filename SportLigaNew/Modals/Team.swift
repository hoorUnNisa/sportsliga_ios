//
//  Team.swift
//  SportLigaNew
//
//  Created by Hoor on 8/18/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Team: Mappable {
    
    var teamId: Int?
    var name: String?
    var country: String?
    var description: String?
    var image: String?
    var favorite: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        teamId <- map["teamId"]
        name <- map["name"]
        country <- map["country"]
        description <- map["description"]
        image <- map["image"]
        favorite <- map["favorite"]
        
    }
    
    
}
