//
//  Poll.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 9/27/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class PollDetail : Mappable{
    var poll_id : Int?
    var poll_title : String?
    var poll_tournament_id : Int?
    var poll_start_date : Date?
    var poll_end_date : String?
    var poll_competitors : [Competitor] = []
    var user_vote : UserVote?
    var winner_index : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        poll_id <- map["id"]
        poll_title <- map["title"]
        poll_tournament_id <- map["tournament_id"]
        poll_start_date <- map["start_date"]
        poll_end_date <- map["end_date"]
        poll_competitors <- map["competitors"]
        user_vote <- map["user_vote"]
        winner_index <- map["winnder_index"]
        
    }
    
    
}

