//
//  Comment.swift
//  SportLigaNew
//
//  Created by Admin on 04/02/1440 AH.
//  Copyright © 1440 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
    var user_id: Int?
    var text: String?
    var against_user: String?
    var commentsUserImgUrl:String?
    var commentsUserName:String?
    var commentId:Int?
    var commmentsUserId:Int?
    var comments:String?
    var likesUserId:Int?
    var likesCommentId:Int?
    var reply_id : Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        against_user <- map["against_user"]
        commentsUserImgUrl     <- map["userImg"]
        commentsUserName       <- map["userName"]
        commentId              <- map["commentId"]
        commmentsUserId        <- map["userId"]
        comments               <- map["comment"]
        likesUserId            <- map["likeUserId"]
        likesCommentId         <- map["likeCommentId"]
        reply_id                <- map["reply_id"]
    }
    
    
}
