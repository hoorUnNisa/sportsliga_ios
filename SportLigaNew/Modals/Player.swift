//
//  Player.swift
//  SportLigaNew
//
//  Created by Hoor on 9/26/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Player:Mappable{
    
    var name : String?
    var image : String?
    var player_id : Int?
    var voteCount : Int?
    var team_name : String?
    //var votes : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        image <- map["thumb_pic"]
        player_id <- map["player_id"]
        voteCount <- map["votesCount"]
        team_name <- map["team_name"]
    }
    
    
}
