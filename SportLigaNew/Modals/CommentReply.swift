//
//  CommentReply.swift
//  SportLigaNew
//
//  Created by Zahid on 10/11/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper
class CommentReply: Mappable{
    
    var user_id: Int?
    var comment_id: Int?
    var text: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        comment_id <- map["comment_id"]
        text <- map["text"]
        
}
}


