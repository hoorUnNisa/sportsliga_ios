//
//  News.swift
//  SportLigaNew
//
//  Created by Tahir on 10/13/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class News : Mappable{
    var id : Int?
    var title : String?
    var image : String?
    var description : String?
    var publishedAt : String?
    var content : String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map ["id"]
        title <- map["title"]
        image <- map["image"]
        description <- map["description"]
        publishedAt <- map["publishedAt"]
        content <- map["content"]
    }
    
    
}
