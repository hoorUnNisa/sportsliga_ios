//
//  Competitors.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/16/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Competitor:Mappable{
    var competitor_id : Int?
    var competitor_name : String?
    var competitor_votes : Int?
    var competitor_image : String?
    var competitor_type : String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        competitor_id <- map["competitor_id"]
        competitor_name <- map["competitor_name"]
        competitor_votes <- map["competitor_votes"]
        competitor_image <- map["competitor_image"]
        competitor_type <- map["competitor_type"]
    }
    
    
}

