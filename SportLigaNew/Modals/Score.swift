//
//  Score.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/4/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Score : Mappable{
    var score_time : String?
    var score_player_name : String?
    var score_team : String?
    var score_type : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        score_team <- map["score_team"]
        score_player_name <- map["score_player"]
        score_type <- map["score_type"]
        score_time <- map["score_time"]
    }
    
    
}
