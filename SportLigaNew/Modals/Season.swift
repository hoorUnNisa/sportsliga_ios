//
//  Season.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/18/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class Season:Mappable{
    var season : String?
    var season_id : Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        season <- map["season"]
        season_id <- map["id"]
    }
    
    
}

