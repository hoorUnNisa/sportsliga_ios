//
//  SearchPoll.swift
//  SportLigaNew
//
//  Created by Ali Iqbal on 10/19/18.
//  Copyright © 2018 Ali Iqbal. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchPoll:Mappable{
    var season : [Season] = []
    var poll : [Poll] = []
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        season <- map["seasons"]
        poll <- map["titles"]
    }
    
    
}

